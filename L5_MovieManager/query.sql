
#select all movies
select 
    title,
    runtime,
    lang,
    production_year as year,
    id_imbd as imbd
from
    catalog_movies.Movie;

#select specified movie

select 
    m.title,
    m.runtime,
    m.lang,
    m.production_year as year,
    m.id_imbd as imbd
from
    catalog_movies.Movie as m
		where m.id = 1;

#select all actors
select  id, 
		aname 
	from catalog_movies.Actor;

#select id actor
select  id
	from catalog_movies.Actor
		where aname = 'Vin Diesel';

#select all directors
select  id, 
		dname 
	from catalog_movies.Director;

#select  director
select  id, 
		dname 
	from catalog_movies.Director
		where dname='James Wan';

#select all categories
select  id, 
		category 
	from catalog_movies.Category;

#select all movie in depends of category
select 
    c.category,
    m.title,
    m.runtime,
    m.lang,
    m.production_year as year,
    m.id_imbd as imbd
from
    catalog_movies.Category as c join
    catalog_movies.movie_category as m_c join
    catalog_movies.Movie as m 
				ON (c.id = m_c.id_category
					&& m_c.id_movie = m.id
					&& c.category = 'Action');

#select all movie in depends of actor
select 
    a.aname as actor,
    m.title,
    m.runtime,
    m.lang,
    m.production_year as year,
    m.id_imbd as imbd
from
    catalog_movies.Actor as a join
    catalog_movies.movie_actor as m_a join
    catalog_movies.Movie as m 
				ON (a.id = m_a.id_actor
					&& m_a.id_movie = m.id
					&& a.aname = 'Vin Diesel');

#select all movie in depends of director
select 
    d.dname as director,
    m.title,
    m.runtime,
    m.lang,
    m.production_year as year,
    m.id_imbd as imbd
from
    catalog_movies.Director as d join
    catalog_movies.Movie as m 
		ON (d.id = m.id_Director
			&& d.dname = 'James Wan');

delete from catalog_movies.movie_actor where movie_actor.id_movie=1;
delete from catalog_movies.movie_category where movie_category.id_movie=1;
delete from catalog_movies.Movie where Movie.id=1;
