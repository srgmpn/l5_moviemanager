Movie Manager
/***/
    Aplicatie de gestiune a filmelor, permite vizualizarea listei de filme
    listei de filme dupa o anumita categorie, anumit actor sau anumit regizor
    afisindu-se prin selectare elementului respectiv, la fel la selectarea unui film 
    se va afisa o lista cu toata informatia existenta despre film, adaugarea unui film se
    va face prin introducerea Numelui fimului si optional a anului in care a fost lansat 
    sau a id IMDB(cale sigura), astfel se va incarca toata informatia despre film de pe site
    imdb va fi parsata si apoi afisata
**/

Arhitectura si interfata proiect

1 Mediu de dezvoltare
2 Implementare
3 Git
4 Maven
5 MySql

1. MEDIU DE DEZVOLTARE  
 - Limbajul de programare : Java 1.7
 - Mediu de dezvoltare : Netbeans
 
2. IMPLEMENTARE

  2.1 Modulele principale
    * GUI
    * Mediator
    * Dao
    
  2.2 Arhitectura aplicatiei
    
    Clasa Mediator se afla in mijlocul legaturilor dintre componente, ea acces la toate celelalte componente si 
    realizeaza schimbul de date intre acestea. Toate celelalte componente (GuiMain, DataManager, ExportPdf, ExportXML), 
    interactioneaza prin intermediul Mediator (Este implementat patternul singleton si se instantiaza doar o singura 
    instata care este transmisa catre toate celelalte component cimpul med, astfel se aplica patternul mediator astfel 
    incit daca se doreste adaugarea ulterioara a unui nou modul sa se faca doar conectare la mediator). Patternul 
    Command a fost utilizat pentru a putea executa o anumita comanda la momentul aparitiei unei actiuni a utilizatorului. 
    La pornirea alplicatie se incarca toate inscrierile existente in baza de date(pentru a crea baza de date se ruleaza script.sql) 
    astfel urmatoarele modificari asupra datelor se vor face si in baza de date(adaugari, stergeri). Toate actiunile 
    legate de baza de date sunt gestionate de DataManager, actiunile de export pdf si export xml sunt rulate pe cite un 
    fir de executie separat de firul main
    
  2.3 Gui (interfata realizata cu JFC Swing)

    In aceasta etapa, pentru handler-ele de evenimente am folosit Event Dispatch Thread (EDT).
    Toate elementele grafice sunt implementate in clasa GuiMain, fiecare actiune a unui button a fost separata in cite o clasa
    packetul com.srg.buttons, iar a ferestrei de dialog sau implementat in clasa AddDialog.

  2.4 Dao (Data access object)
    
    Pentru a efectua legatura dintre baza de date si aplicatie am implementat patternul dao, astfel am creat interfatele
    Dao si creat pentru fiecare clasa pojo implemetarea in parte, pentru a obtine obiecte de tip dao sa implementat pattenrul
    factory Method, operatiile implemetate sunt cele de delete, insert
    Pentru a facilita crearea de conxiui la db am utlizat BasicDataSource clasa a packetului Apache Commons DBCP pentru a configura
    elemente de baza pentru conexiunea la baza de date.

 2.5 HttpClient

    Se utilizeaza clasa apache HttpClient pentru a trimite un GET request spre adresa http://www.omdbapi.com/?, cu parametri 
    necesare care intoarce ca raspuns un streem de date in format json, datele fimlului respectiv, bineinteles daca 
    adresa este valida.

 2.6 ExportXML 
    
    Se utilizeaza StAX pentru a crea documentul xml cu date despre filme.

 2.7 ExportPdf

    Pentru a genera un fisier pdf cu date despre filme sa utilizat JasperReports.

 2.8 Logging 

    Pentru jurnalizarea mesajelor am folosit nivele diferite de granularitate(error pentru exceptii, info si warn pentru mesajele obisnuite).

2.9 Testare
    
    Pentru validarea functionalitatii legate de inscrierea datelor in fisier si transmiterea requestului, primirea, validarea
    si parsarea raspunsului  am folosit unit testing(JUnit).
    Astfel sunt realizate unit test-uri pentru clasele din pachetul com.srg.dao.DaoImpl, com.srg.dates.json si com.srg.httpclient

3. GIT    
    
    Pentru version control, am folosit Git

4. MAVEN
    
    Pentru a incarca toate librariile de care avem nevoie 
     in program am utilizat Maven 3.0.5
    
5. MYSQL
    
    Pentru lucru cu baze de date sa utilizat serverul mysql 
    versiunea de conector este specificata in lista 
    de dependente maven
    
    
    
