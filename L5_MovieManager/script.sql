CREATE DATABASE IF NOT EXISTS catalog_movies;

USE catalog_movies;

CREATE TABLE IF NOT EXISTS Actor(
	id 				INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	aname   		VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS Category(
	id 				INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	category   		VARCHAR(15) NOT NULL
);

CREATE TABLE IF NOT EXISTS Director(
	id 				INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	dname   		VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS Movie(
	id 				INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	title   		VARCHAR(50) NOT NULL UNIQUE,
	id_imbd 		VARCHAR(10) NOT NULL UNIQUE,
	production_year YEAR(4) NOT NULL,
	runtime 		VARCHAR(20) NOT NULL,
	lang			VARCHAR(100) NOT NULL,
	url_json		VARCHAR(1000) NOT NULL,
	id_Director		INT(6) UNSIGNED NOT NULL
);

ALTER TABLE Movie ADD CONSTRAINT id_Director_FK FOREIGN KEY(id_Director) REFERENCES Director(id);

CREATE TABLE IF NOT EXISTS movie_actor(
	id 				INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_movie 		INT(6) UNSIGNED NOT NULL,
	id_actor		INT(6) UNSIGNED NOT NULL
);

ALTER TABLE movie_actor ADD CONSTRAINT id_movie_FKMA FOREIGN KEY(id_movie) REFERENCES Movie(id);
ALTER TABLE movie_actor ADD CONSTRAINT id_actor_FKMA FOREIGN KEY(id_actor) REFERENCES Actor(id);

CREATE TABLE IF NOT EXISTS movie_category(
	id 				INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
	id_movie 		INT(6) UNSIGNED NOT NULL,
	id_category		INT(6) UNSIGNED NOT NULL
);

ALTER TABLE movie_category ADD CONSTRAINT id_movie_FKMC FOREIGN KEY(id_movie) REFERENCES Movie(id);
ALTER TABLE movie_category ADD CONSTRAINT id_actor_FKMC FOREIGN KEY(id_category) REFERENCES Category(id);

INSERT INTO catalog_movies.Director (dname) VALUES
('James Wan'),
('Claudia Llosa'),
('Peter Jackson');

INSERT INTO catalog_movies.Movie (title,id_imbd,production_year,runtime,lang,url_json,id_Director) 
	VALUES
		('Furious Seven','tt2820852',2015,'137 min','English','{"Title":"Furious Seven","Year":"2015","Rated":"PG-13","Released":"3 Apr 2015","Runtime":"137 min","Genre":"Action, Crime, Thriller","Director":"James Wan","Writer":"Chris Morgan, Gary Scott Thompson (characters)","Actors":"Vin Diesel, Paul Walker, Jason Statham, Michelle Rodriguez","Plot":"Deckard Shaw seeks revenge against Dominic Toretto and his family for his comatose brother.","Language":"English","Country":"USA, Japan","Awards":"N/A","Poster":"http://ia.media-imdb.com/images/M/MV5BMTQxOTA2NDUzOV5BMl5BanBnXkFtZTgwNzY2MTMxMzE@._V1_SX300.jpg","Metascore":"67","imdbRating":"7.9","imdbVotes":"129135","imdbID":"tt2820852","Type":"movie","Response":"True"}',1),
		('Aloft','tt2494384',2014,'112 min','English','{"Title":"Aloft","Year":"2014","Rated":"R","Released":"23 Jan 2015","Runtime":"112 min","Genre":"Drama","Director":"Claudia Llosa","Writer":"Claudia Llosa (screenplay)","Actors":"Jennifer Connelly, Cillian Murphy, Oona Chaplin, Mélanie Laurent","Plot":"Aloft tells of a struggling mother who encounters the son she abandoned 20 years earlier.","Language":"English","Country":"Spain, Canada, France","Awards":"1 win & 1 nomination.","Poster":"http://ia.media-imdb.com/images/M/MV5BMjAyMzY2MTAxOF5BMl5BanBnXkFtZTgwOTQwOTEzNTE@._V1_SX300.jpg","Metascore":"39","imdbRating":"6.4","imdbVotes":"276","imdbID":"tt2494384","Type":"movie","Response":"True"}',2),
		('The Hobbit: An Unexpected Journey','tt0903624',2012,'169 min','English','{"Title":"The Hobbit: An Unexpected Journey","Year":"2012","Rated":"PG-13","Released":"14 Dec 2012","Runtime":"169 min","Genre":"Adventure, Fantasy","Director":"Peter Jackson","Writer":"Fran Walsh (screenplay), Philippa Boyens (screenplay), Peter Jackson (screenplay), Guillermo del Toro (screenplay), J.R.R. Tolkien (novel)","Actors":"Ian McKellen, Martin Freeman, Richard Armitage, Ken Stott","Plot":"A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home - and the gold within it - from the dragon Smaug.","Language":"English","Country":"USA, New Zealand","Awards":"Nominated for 3 Oscars. Another 12 wins & 58 nominations.","Poster":"http://ia.media-imdb.com/images/M/MV5BMTcwNTE4MTUxMl5BMl5BanBnXkFtZTcwMDIyODM4OA@@._V1_SX300.jpg","Metascore":"58","imdbRating":"8.0","imdbVotes":"552675","imdbID":"tt0903624","Type":"movie","Response":"True"}',3),
		('The Conjuring','tt1457767',2013,'112 min','English','{"Title":"The Conjuring","Year":"2013","Rated":"R","Released":"19 Jul 2013","Runtime":"112 min","Genre":"Horror","Director":"James Wan","Writer":"Chad Hayes, Carey Hayes","Actors":"Vera Farmiga, Patrick Wilson, Lili Taylor, Ron Livingston","Plot":"Paranormal investigators Ed and Lorraine Warren work to help a family terrorized by a dark presence in their farmhouse.","Language":"English, Latin","Country":"USA","Awards":"17 wins & 15 nominations.","Poster":"http://ia.media-imdb.com/images/M/MV5BMTM3NjA1NDMyMV5BMl5BanBnXkFtZTcwMDQzNDMzOQ@@._V1_SX300.jpg","Metascore":"68","imdbRating":"7.5","imdbVotes":"237240","imdbID":"tt1457767","Type":"movie","Response":"True"}',1);

INSERT INTO catalog_movies.Category (category) 
	VALUES
		('Action'),
		('Thriller'),
		('Crime'),
		('Drama'),
		('Adventure'),
		('Fantasy'),
		('Horror');
		

INSERT INTO catalog_movies.Actor (aname) 
	VALUES
		('Vin Diesel'),
		('Paul Walker'),
		('Michelle Rodriguez'),
		('Jason Statham'),
		('Jennifer Connelly'),
		('Cillian Murphy'),
		('Oona Chaplin'),
		('Mélanie Laurent'),
		('Ian McKellen'),
		('Martin Freeman'),
		('Richard Armitage'),
		('Ken Stott'),
		('Vera Farmiga'),
		('Patrick Wilson'),
		('Lili Taylor'),
		('Ron Livingston');


INSERT INTO catalog_movies.movie_actor (id_movie,id_actor)
	VALUES
		(1,1),
		(1,2),
		(1,3),
		(1,4),
		(2,5),
		(2,6),
		(2,7),
		(2,8),
		(3,9),
		(3,10),
		(3,11),
		(3,12),
		(4,13),
		(4,14),
		(4,15),
		(4,16);

INSERT INTO catalog_movies.movie_category (id_movie,id_category) 
	VALUES
		(1,1),
		(1,2),
		(1,3),
		(2,4),
		(3,5),
		(3,6),
		(4,7);


