/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.models;

import java.util.List;
import java.util.Map;
import javax.swing.tree.TreePath;

/**
 *
 * @author administrator
 */
public class MapTreeModel extends AbstractTreeModel{
    
    private Map map;
    private Object root;
    
    public MapTreeModel(){
        this(null, null);
    }
    
    public MapTreeModel(Map map, Object root){
        setMap(map);
        setRoot(root);
    }
    
    public void setMap(Map map){
        this.map = map;
    }
    
    public void setRoot(Object root){
        this.root = root;
    }
    
    @Override
    public Object getRoot() {
        return root;
    }

    @Override
    public Object getChild(Object parent, int index) {
        return ((List)map.get(parent)).get(index);
    }

    @Override
    public int getChildCount(Object parent) {
        List children = (List)map.get(parent);
        
        return ((children == null) ? 0 : children.size());    
    }

    @Override
    public boolean isLeaf(Object node) {
        return !map.containsKey(node);
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        return -1;
    }
}
