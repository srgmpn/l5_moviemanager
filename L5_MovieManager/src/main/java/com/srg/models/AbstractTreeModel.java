/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.models;

import javax.swing.tree.TreeModel;

/**
 * @author administrator
 * 
 * 
 * This class takes care of the event listener lists required by TreeModel.
 * It also adds "fire" methods that call the methods in TreeModelListener.
 * Look in TreeModelSupport for all of the pertinent code.
 * 
 */
public abstract class AbstractTreeModel extends TreeModelSuport implements TreeModel{
    
}
