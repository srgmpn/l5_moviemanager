/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.models;

import com.srg.pojo.Movie;
import static com.srg.util.Constants.NAME_COL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author administrator
 */
public class JTableModel extends AbstractTableModel {
    
    protected String [] columnNames = NAME_COL;

    protected List  list = new ArrayList();
    
    protected Class[] columnClasses = new Class[]{
          String.class, String.class, String.class, String.class, String.class };
    
    public JTableModel(){
    }
    
    public void add(Object element){
        list.add(element);
        fireTableRowsInserted(0, list.size());
    }   
    
    public void add(Collection objects){
        list.clear();
        for (Object m : objects){
            add(m);
        }
        fireTableRowsInserted(0, list.size());

    } 
      
    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
                
        Movie m = (Movie)list.get(rowIndex);
        
        switch (columnIndex){
            case 0:
                return m.getTitle();
            case 1:
                return m.getYear();
            case 2:
                return m.getLang();
            case 3: 
                return m.getRuntime();
            case 4:
                return m.getImbd();
            default: return null;
        }
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }
    
     @Override
    public void setValueAt(Object value, int row, int col) {
        Movie m = (Movie)list.get(row);

        switch (col) {
            case 0:
                 m.setTitle((String) value);
            case 1:
                 m.setYear((String) value);
            case 2:
                 m.setLang((String) value);
            case 3:
                 m.setRuntime((String) value);
            case 4:
                 m.setImbd((String) value);
        }

        list.set(row, m);
        fireTableCellUpdated(row, col);
    }   
    
    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }
    
    @Override
    public Class getColumnClass(int col) {
        return columnClasses[col];
    }
    
    public Object getRowValue(int row){
        return list.get(row);
    }
}
