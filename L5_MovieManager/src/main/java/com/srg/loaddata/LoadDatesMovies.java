/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.loaddata;

import com.srg.dao.Dao;
import com.srg.dao.impl.DaoFactory;
import com.srg.pojo.Movie;
import com.srg.util.CommandsDao;
import static com.srg.util.Constants.MOVIE;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;


/**
 *
 * @author administrator
 */
public class LoadDatesMovies implements LoadDates<Movie>{
    
    private Map map;
    private Map  movieMap;
    
   final static Logger logger = Logger.getLogger(LoadDatesMovies.class);
    
    public LoadDatesMovies(Map map, Map movieMap){
        this.map = map;
        this.movieMap = movieMap;
    }
      
    @Override
    public void load() {
        logger.info("load Movie");
        List<Movie> ms = getExistingMovies();
        
        if (ms != null){
            
             List movies = new ArrayList();
            
            for (Movie m : ms){
                movieMap.put(m.getTitle(), m);
                movies.add(m.getTitle());
            }
            
            map.put(MOVIE, movies);
        } else{
            map.put(MOVIE, Collections.EMPTY_LIST);
            
        }      
        
    }

    @Override
    public void loadMovies(Movie t) {
    }
    
     /**
     * select all movies is DB
     * @return list with movies
     */       
     private List<Movie> getExistingMovies(){

        List<Movie> movies = null;
        Dao query = DaoFactory.getInstanceDaoFactory().getDao(CommandsDao.MOVIE);
        try {
            movies = query.getAll();
        } catch (SQLException ex) {
            logger.error("getExistingMovies " + ex);
        }
        
        return movies;        
    }    
}
