/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.loaddata;

import com.srg.dao.Dao;
import com.srg.dao.impl.DaoFactory;
import static com.srg.loaddata.LoadDatesCategories.logger;
import com.srg.pojo.Director;
import com.srg.pojo.Movie;
import com.srg.util.CommandsDao;
import static com.srg.util.Constants.DIRECTOR;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;


/**
 *
 * @author administrator
 */
public class LoadDatesDirectors implements LoadDates<Director>{
    
    private Map map;
    private Dao query;
    private Map directorMap;
    final static Logger logger = Logger.getLogger(LoadDatesDirectors.class);
    
    public LoadDatesDirectors(Map map, Map directorMap){
        this.map = map;
        this.directorMap = directorMap;
        query = DaoFactory.getInstanceDaoFactory().getDao(CommandsDao.DIRECTOR);
    }
    


    @Override
    public void load() {
        logger.info("load Director");
        List<Director> ds = getExistingDirectors();
        
        if (ds != null){
            
             List directors = new ArrayList();
            
            for (Director d : ds){
                directorMap.put(d.getName(), d);
                directors.add(d.getName());
                loadMovies(d);
            }
            map.put(DIRECTOR, directors);
            
        } else{
             map.put(DIRECTOR, Collections.EMPTY_LIST);
        }   
    }

    @Override
    public void loadMovies(Director t) {
 
        List<Movie> ms = getAllExistingMovieOfDirector(t);
        
        if (ms != null){
            List movies = new ArrayList();
            for (Movie m : ms){
                movies.add(m.getTitle());
            }
            map.put(t.getName(), movies);
            
        } else {
            map.put(t.getName(), Collections.EMPTY_LIST);
   
        }      
    }

    /**
     * select all directories is DB
     * @return list with directories
     */      
     private List<Director> getExistingDirectors(){

        List<Director> directors = null;
        try {
            directors = query.getAll();
        } catch (SQLException ex) {

            logger.error("getExistingDirectors " + ex);
        }
        
        return directors;        
    }  
     
    /**
     * get all movies of selected 
     * @param director - selected director
     * @return - list movies
     */
    private List<Movie> getAllExistingMovieOfDirector(Director director){
        List<Movie> list = null;
        
        try {
            Director d =  (Director) query.getAllMovie(director.getName());
            list = d.getMovies();
        } catch (SQLException ex) {

            logger.error("getAllExistingMovieOfDirector " + ex);
        }
        return list;
    }      
}
