/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.loaddata;

import com.srg.dao.Dao;
import com.srg.dao.impl.DaoFactory;
import static com.srg.loaddata.LoadDatesCategories.logger;
import com.srg.pojo.Actor;
import com.srg.pojo.Movie;
import com.srg.util.CommandsDao;
import static com.srg.util.Constants.ACTOR;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;


/**
 *
 * @author administrator
 */
public class LoadDatesActors implements LoadDates<Actor>{
    
    private Dao query;
    private Map  map;
    private Map actorsMap;
    
    final static Logger logger = Logger.getLogger(LoadDatesActors.class);
    
    public LoadDatesActors(Map map, Map actorsMap){
        query = DaoFactory.getInstanceDaoFactory().getDao(CommandsDao.ACTOR); 
        this.map = map;
        this.actorsMap = actorsMap;
    }
     

    @Override
    public void load() {
        logger.info("load Actor");
        List<Actor> acts = getExistingActors();
        
        if (acts != null){
            
             List actors = new ArrayList();
            
            for (Actor a : acts){
                actors.add(a.getName());
                actorsMap.put(a.getName(), a);
                loadMovies(a);
            }
            map.put(ACTOR, actors);
            
        } else{
            map.put(ACTOR, Collections.EMPTY_LIST);
            
        }      
    }

    @Override
    public void loadMovies(Actor t) {
        List<Movie> ms = getAllExistingMovieOfActor(t);
        
        if (ms != null){
            List movies = new ArrayList();
            for (Movie m : ms){
                movies.add(m.getTitle());
            }
            map.put(t.getName(), movies);
            
        } else {
            map.put(t.getName(), Collections.EMPTY_LIST);
            
        }
    }
    
    /**
     * select all actors is DB
     * @return list with actors
     */
    private List<Actor> getExistingActors(){

        List<Actor> actors = null;
        try {
            actors = query.getAll();
        } catch (SQLException ex) {
            logger.error("getExistingActors " + ex);
        }
        
        return actors;        
    }
    
    /**
     * get all movies of selected 
     * @param actor - selected actor
     * @return - list movies
     */
    private List<Movie> getAllExistingMovieOfActor(Actor actor){
        List<Movie> list = null;
        
        try {
            Actor a =  (Actor) query.getAllMovie(actor.getName());
            list = a.getMovies();
        } catch (SQLException ex) {

            logger.error("getAllExistingMovieOfActor " + ex);
        }
        return list;
    }    
}
