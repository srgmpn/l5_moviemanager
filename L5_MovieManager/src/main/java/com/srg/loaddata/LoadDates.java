/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.loaddata;

/**
 *
 * @author administrator
 */
public interface LoadDates<T> {
    public void load();
    public void loadMovies(T t);
}
