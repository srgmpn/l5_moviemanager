/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.loaddata;

import com.srg.dao.Dao;
import com.srg.dao.impl.DaoFactory;
import com.srg.pojo.Category;
import com.srg.pojo.Movie;
import com.srg.util.CommandsDao;
import static com.srg.util.Constants.CATEGORY;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;


/**
 *
 * @author administrator
 */
public class LoadDatesCategories implements LoadDates<Category>{
    
    private Map map;
    private Dao query ;
    private Map categoryMap;
    final static Logger logger = Logger.getLogger(LoadDatesCategories.class);
    
    public LoadDatesCategories(Map map, Map categoryMap){
        this.map = map;
        query = DaoFactory.getInstanceDaoFactory().getDao(CommandsDao.CATEGORY);
        this.categoryMap = categoryMap;
    }
    
  

    @Override
    public void load() {
        logger.info("load Category");
        List<Category> cs = getExistingCategories();
        
        if (cs != null){
            
             List categories = new ArrayList();
            
            for (Category c : cs){
                categoryMap.put(c.getCategory(), c);
                categories.add(c.getCategory());
                loadMovies(c);
            }
            map.put(CATEGORY,  categories);
            
        } else{
            map.put(CATEGORY, Collections.EMPTY_LIST);
        } 
    }

    @Override
    public void loadMovies(Category t) {
        List<Movie> ms = getAllExistingMovieOfCategory(t);
        
        if (ms != null){
            List movies = new ArrayList();
            for (Movie m : ms){
                movies.add(m.getTitle());
            }
            map.put(t.getCategory(), movies);
            
        } else {
            map.put(t.getCategory(), Collections.EMPTY_LIST);
   
        } 
    }
    
     /**
     * select all categories is DB
     * @return list with categories
     */   
    private List<Category> getExistingCategories(){

        List<Category> categories = null;
        
        try {
            categories = query.getAll();
        } catch (SQLException ex) {

            logger.error("getExistingCategories " + ex);
        }
        
        return categories;        
    }  
 
     /**
     * get all movies of selected 
     * @param category - selected category
     * @return - list movies
     */
    private List<Movie> getAllExistingMovieOfCategory(Category category){
        List<Movie> list = null;
        
        try {
            Category c = (Category) query.getAllMovie(category.getCategory());
            list = c.getMovies();
        } catch (SQLException ex) {
           logger.error("getAllExistingMovieOfCategory " + ex);
        }
        return list;
    }   
}
