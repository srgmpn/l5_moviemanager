/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.dao;

import java.sql.SQLException;

/**
 *
 * @author administrator
 */
public interface DaoID<T>{ 
    public void insert(T t) throws SQLException;
    public void delete(T t) throws SQLException;
}
