/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.dao.impl;

import com.srg.dao.DaoID;
import com.srg.data.DataSource;
import com.srg.pojo.MovieActor;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author administrator
 */
public class MovieActorDao implements DaoID<MovieActor>{

    @Override
    public void insert(MovieActor t) throws SQLException {
        DataSource dataSource = new DataSource(); 
        Connection conn = null;
        Statement stmt = null;
        String sql = "insert into movie_actor(id_movie,id_actor) values("
                                +  t.getIdMovie()+ "," + t.getIdActor() + ");";
        
        try{
            conn = dataSource.createConnection();
            stmt = conn.prepareStatement(sql);
            stmt.executeUpdate(sql);
            
        } finally {
            if( conn != null )
                conn.close();
        
            if( stmt != null )
                stmt.close();  
        }
    }

    @Override
    public void delete(MovieActor t) throws SQLException {
        DataSource dataSource = new DataSource(); 
        Connection conn = null;
        Statement stmt = null;
        
        String sql = "delete from movie_actor where id_movie="  + t.getIdMovie()+ ";";
        try{
              
            conn = dataSource.createConnection();
            stmt = conn.prepareStatement(sql);
            stmt.executeUpdate(sql);
            
        } finally {
            if( conn != null )
                conn.close();
        
            if( stmt != null )
                stmt.close();  
        }           
    } 
}
