/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.dao.impl;

import com.srg.dao.Dao;
import com.srg.data.DataSource;
import com.srg.pojo.Director;
import com.srg.pojo.Movie;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javafx.collections.transformation.SortedList;

/**
 *
 * @author administrator
 */
public class DirectorDao implements Dao<Director>{

    @Override
    public void insert(Director t) throws SQLException {
        
        DataSource dataSource = new DataSource(); 
        Connection conn = null;
        Statement stmt = null;
        String sql = "insert into Director(dname) values(" + "'" + t.getName() + "'" + ");";
        try{
              
            conn = dataSource.createConnection();
            stmt = conn.prepareStatement(sql);
            stmt.executeUpdate(sql);
            
        } finally {
            if( conn != null )
                conn.close();
        
            if( stmt != null )
                stmt.close();  
        }
    }

    @Override
    public Director get(String n) throws SQLException {
        
        DataSource dataSource = new DataSource();   
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String query = "Select * from Director where dname=" + "'" + n +"'" + ";";
        
        try{ 
            conn = dataSource.createConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            Director d = new Director();
            while (rs.next()){
                
                d.setId(rs.getInt(1));
                d.setName(rs.getString(2));
                
            }
            return d;
        } finally {
            if( conn != null )
                conn.close();
        
            if( stmt != null )
                stmt.close();
            
            if( rs != null )
                rs.close();   
        }
    }

    @Override
    public List<Director> getAll() throws SQLException {
        
        DataSource dataSource = new DataSource();
        Connection conn = dataSource.createConnection();
        Statement stmt = null;
        ResultSet rs = null;
        List<Director> list = new ArrayList<>();
        
        String query = "Select * from Director";
        try{
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
        
            while (rs.next()){
                Director d = new Director();
                d.setId(rs.getInt(1));
                d.setName(rs.getString(2));
                d.setMovies(null);
                list.add(d);
            }
            
        } finally{
            if( conn != null )
                conn.close();
        
            if( stmt != null )
                stmt.close();
            
            if( rs != null )
                rs.close();
        }  
        return list;
    }

    @Override
    public void delete(Director t) throws SQLException {
        DataSource dataSource = new DataSource(); 
        Connection conn = null;
        Statement stmt = null;
        
        String sql = "delete from Director where id="  + t.getId() + ")";
        try{
              
            conn = dataSource.createConnection();
            stmt = conn.prepareStatement(sql);
            stmt.executeUpdate(sql);
            System.out.println("Record is deleted Director : " + t.getName());
        } finally {
            if( conn != null )
                conn.close();
        
            if( stmt != null )
                stmt.close();  
        }       
    }

    @Override
    public Director getAllMovie(String n) throws SQLException {
        DataSource dataSource = new DataSource();   
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        Calendar cal = Calendar.getInstance();
        
        String query = "Select d.id,"
                        + "d.dname,"
                        + "m.id,"
                        + "m.title,"
                        + "m.runtime,"
                        + "m.lang,"
                        + "m.production_year,"
                        + "m.id_imbd,"
                        + "m.id_Director"
                + " from Director as d join "
                + "Movie as m"
                + " ON (d.id=m.id_Director && d.dname=" + "'" + n +"'"+");";
        
        List<Movie> list = new ArrayList<>();
        Director d = new Director();
        try{ 
            conn = dataSource.createConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            
            while (rs.next()){
             
                d.setId(rs.getInt(1));
                d.setName(rs.getString(2));
                
                Movie m = new Movie();
                m.setId(rs.getInt(3));
                m.setTitle(rs.getString(4));
                m.setRuntime(rs.getString(5));
                m.setLang(rs.getString(6));
                cal.setTime(Date.valueOf(rs.getString(7)));
                m.setYear(""+cal.get(Calendar.YEAR));
                m.setImbd(rs.getString(8));
                m.setIdDirector(rs.getInt(9));
                
                list.add(m);
                
            }
            d.setMovies(list);
        } finally {
            if( conn != null )
                conn.close();
        
            if( stmt != null )
                stmt.close();
            
            if( rs != null )
                rs.close();            
        }
        return d;
    }
    
}
