/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.dao.impl;

import com.srg.dao.DaoID;
import com.srg.util.CommandsDaoID;

/**
 *
 * @author administrator
 */
public class DaoIDFactory {
    private static DaoIDFactory INSTANCE;
    
    private DaoIDFactory(){}
    
    public static DaoIDFactory getInstanceDaoFactory(){
        
        if (INSTANCE == null){
            INSTANCE = new DaoIDFactory();
        }
        return INSTANCE;
    }
    
    public DaoID getDaoID(CommandsDaoID cmd){
        
        switch (cmd){
            case MOVIE_ACTOR:
                return new MovieActorDao();
            
            case MOVIE_CATEGORY: 
                return new MovieCategoryDao();

        }
        return null;
    }    
}
