/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.dao.impl;

import com.srg.dao.DaoID;
import com.srg.data.DataSource;
import com.srg.pojo.MovieCategory;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author administrator
 */
public class MovieCategoryDao implements DaoID<MovieCategory> {

    @Override
    public void insert(MovieCategory t) throws SQLException {
        
        DataSource dataSource = new DataSource(); 
        Connection conn = null;
        Statement stmt = null;
        
        String sql = "insert into movie_category (id_movie,id_category)  values("
                                +  t.getIdMovie()+ "," + t.getIdCategory() + ");";
        try{
            
            conn = dataSource.createConnection();
            stmt = conn.prepareStatement(sql);
            stmt.executeUpdate(sql);
            
        } finally {
            if( conn != null )
                conn.close();
        
            if( stmt != null )
                stmt.close();  
        }      
    }

    @Override
    public void delete(MovieCategory t) throws SQLException {
        DataSource dataSource = new DataSource(); 
        Connection conn = null;
        Statement stmt = null;
        
        String sql = "delete from movie_category where id_movie="  + t.getIdMovie()+ ";";
        try{
              
            conn = dataSource.createConnection();
            stmt = conn.prepareStatement(sql);
            stmt.executeUpdate(sql);
            
        } finally {
            if( conn != null )
                conn.close();
        
            if( stmt != null )
                stmt.close();  
        }    
    }
    
}
