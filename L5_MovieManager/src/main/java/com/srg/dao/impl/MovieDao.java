/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.dao.impl;

import com.srg.dao.Dao;
import com.srg.data.DataSource;
import com.srg.pojo.Movie;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author administrator
 */
public class MovieDao implements Dao<Movie>{

    @Override
    public void insert(Movie t) throws SQLException {
        DataSource dataSource = new DataSource(); 
        Connection conn = null;
        Statement stmt = null;
        //title,id_imbd,production_year,runtime,lang,id_Director
        String sql = "insert into Movie(title,id_imbd,production_year,runtime,lang,url_json,id_Director) "
                                + "values(" + "'" + t.getTitle() + "',"
                                                + "'" + t.getImbd() + "',"
                                                +  t.getYear()+ ","
                                                + "'" + t.getRuntime() + "',"
                                                + "'" + t.getLang() + "',"
                                                + "'" + t.getJsonUrl() + "',"
                                                + t.getIdDirector() + ");";
        try{
              
            conn = dataSource.createConnection();
            stmt = conn.prepareStatement(sql);
            stmt.executeUpdate(sql);
            
        } finally {
            if( conn != null )
                conn.close();
        
            if( stmt != null )
                stmt.close();  
        }
    }

    @Override
    public Movie get(String n) throws SQLException {
        
        DataSource dataSource = new DataSource();   
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        
        Calendar cal = Calendar.getInstance();
 
        String query = "Select * from Movie where id_imbd=" + "'" +  n  + "'"+";";  
        
        try{ 
            conn = dataSource.createConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            Movie m = new Movie();
            while (rs.next()){      
                
                m.setId(rs.getInt(1));
                m.setTitle(rs.getString(2));
                m.setImbd(rs.getString(3));
                cal.setTime(Date.valueOf(rs.getString(4)));
                m.setYear(""+cal.get(Calendar.YEAR));
                m.setRuntime(rs.getString(5));
                m.setLang(rs.getString(6));
                m.setJsonUrl(rs.getString(7));
                m.setIdDirector(rs.getInt(8));
                                
            } 
            return m;

        } finally {
            if( conn != null )
                conn.close();
        
            if( stmt != null )
                stmt.close();
            
            if( rs != null )
                rs.close(); 
           // return null;
        }
        
    }

    @Override
    public List<Movie> getAll() throws SQLException {
        DataSource dataSource = new DataSource();
        Connection conn = dataSource.createConnection();
        Statement stmt = null;
        ResultSet rs = null;
        
        Calendar cal = Calendar.getInstance();
        
        List<Movie> list = new ArrayList<>();
        String query = "Select * from Movie";
        
        try{
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
        
            while (rs.next()){
                Movie m = new Movie();
                
                m.setId(rs.getInt(1));
                m.setTitle(rs.getString(2));
                m.setImbd(rs.getString(3));
                cal.setTime(Date.valueOf(rs.getString(4)));
                m.setYear(""+cal.get(Calendar.YEAR));
                m.setRuntime(rs.getString(5));
                m.setLang(rs.getString(6));
                m.setJsonUrl(rs.getString(7));
                m.setIdDirector(rs.getInt(8));                

                list.add(m);
            }
            
        } finally{
            if( conn != null )
                conn.close();
        
            if( stmt != null )
                stmt.close();
            
            if( rs != null )
                rs.close();
        }  
        return list;
    }

    @Override
    public Movie getAllMovie(String n) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Movie t) throws SQLException {
        DataSource dataSource = new DataSource(); 
        Connection conn = null;
        Statement stmt = null;
        
        String sql = "delete from Movie where id="  + t.getId() + ";";
        try{
              
            conn = dataSource.createConnection();
            stmt = conn.prepareStatement(sql);
            stmt.executeUpdate(sql);
            
            System.out.println("Record is deleted Movie title: " + t.getTitle());
        } finally {
            if( conn != null )
                conn.close();
        
            if( stmt != null )
                stmt.close();  
        }
    }
}
