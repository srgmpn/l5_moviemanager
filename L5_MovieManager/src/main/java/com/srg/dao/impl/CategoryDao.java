/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.dao.impl;

import com.srg.dao.Dao;
import com.srg.data.DataSource;
import com.srg.pojo.Category;
import com.srg.pojo.Movie;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author administrator
 */
public class CategoryDao implements Dao<Category>{

    @Override
    public void insert(Category t) throws SQLException {
        DataSource dataSource = new DataSource(); 
        Connection conn = null;
        Statement stmt = null;
        String sql = "insert into Category(category) values("
                                + "'" + t.getCategory()+ "'" + ");";
        
        try{
            conn = dataSource.createConnection();
            stmt = conn.prepareStatement(sql);
            stmt.executeUpdate(sql);
            
        } finally {
            if( conn != null )
                conn.close();
        
            if( stmt != null )
                stmt.close();  
        }
    }

    @Override
    public Category get(String n) throws SQLException {
        DataSource dataSource = new DataSource();   
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String query = "Select * from Category where category=" + "'" + n +"'" + ";";
        
        try{ 
            conn = dataSource.createConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            
            Category c = new Category();
            
            while (rs.next()){
                c.setId(rs.getInt(1));
                c.setCategory(rs.getString(2));
            }
            return c;
        } finally {
            if( conn != null )
                conn.close();
        
            if( stmt != null )
                stmt.close();
            
            if( rs != null )
                rs.close();   
        }  
    }

    @Override
    public List<Category> getAll() throws SQLException {
        DataSource dataSource = new DataSource();
        Connection conn = dataSource.createConnection();
        Statement stmt = null;
        ResultSet rs = null;
        List<Category> list = new ArrayList<>();        
        
        String query = "Select * from Category";
        try{
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
        
            while (rs.next()){
                Category c = new Category();
                c.setId(rs.getInt(1));
                c.setCategory(rs.getString(2));
                c.setMovies(null);
                list.add(c);
            }
            
        } finally{
            if( conn != null )
                conn.close();
        
            if( stmt != null )
                stmt.close();
            
            if( rs != null )
                rs.close();
        }  
        return list;          
    }

    @Override
    public Category getAllMovie(String n) throws SQLException {
        DataSource dataSource = new DataSource();   
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        Calendar cal = Calendar.getInstance();
        
        String query = "Select c.id,"
                        + "c.category,"
                        + "m.id,"
                        + "m.title,"
                        + "m.runtime,"
                        + "m.lang,"
                        + "m.production_year,"
                        + "m.id_imbd,"
                        + "m.id_Director"
                + " from Category as c join "
                + "movie_category as m_c join "
                + "Movie as m"
                + " ON (c.id = m_c.id_category && m_c.id_movie = m.id &&"
                    + "c.category=" + "'" + n +"'"+");";
        
        List<Movie> list = new ArrayList<>();
        Category c = new Category();
        
        try{ 
            conn = dataSource.createConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            
            while (rs.next()){
             
                c.setId(rs.getInt(1));
                c.setCategory(rs.getString(2));
                
                Movie m = new Movie();
                m.setId(rs.getInt(3));
                m.setTitle(rs.getString(4));
                m.setRuntime(rs.getString(5));
                m.setLang(rs.getString(6));
                cal.setTime(Date.valueOf(rs.getString(7)));
                m.setYear(""+cal.get(Calendar.YEAR));
                m.setImbd(rs.getString(8));
                m.setIdDirector(rs.getInt(9));
                
                list.add(m);
            }
            
            c.setMovies(list);
            
        } finally {
            if( conn != null )
                conn.close();
        
            if( stmt != null )
                stmt.close();
            
            if( rs != null )
                rs.close();            
        }
        return c;            
    }

    @Override
    public void delete(Category t) throws SQLException {
        DataSource dataSource = new DataSource(); 
        Connection conn = null;
        Statement stmt = null;
        
        String sql = "delete from Category where id="  + t.getId() + ";";
        try{
              
            conn = dataSource.createConnection();
            stmt = conn.prepareStatement(sql);
            stmt.executeUpdate(sql);
            System.out.println("Record is deleted Category : " + t.getCategory());
            
        } finally {
            if( conn != null )
                conn.close();
        
            if( stmt != null )
                stmt.close();  
        }           
    }
    
}
