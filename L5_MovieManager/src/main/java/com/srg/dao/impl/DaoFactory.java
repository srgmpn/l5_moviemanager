/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.dao.impl;

import com.srg.dao.Dao;
import com.srg.util.CommandsDao;

/**
 *
 * @author administrator
 */
public class DaoFactory {
    
    private static DaoFactory INSTANCE;
    
    private DaoFactory(){}
    
    public static DaoFactory getInstanceDaoFactory(){
        
        if (INSTANCE == null){
            INSTANCE = new DaoFactory();
        }
        return INSTANCE;
    }
    
    public Dao getDao(CommandsDao cmd){
        
        switch (cmd){
            case ACTOR:
                return new ActorDao();
            
            case CATEGORY: 
                return new CategoryDao();
            
            case DIRECTOR:
                return new DirectorDao();
            
            case MOVIE:
                return new MovieDao();
        }
        
        return null;
    }
}
