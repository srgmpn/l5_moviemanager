/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.dao;

import com.srg.pojo.Movie;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author administrator
 */
public interface Dao<T> extends DaoID<T>{
    public T get(String n) throws SQLException;
    public List<T> getAll() throws SQLException;
    public T getAllMovie(String n) throws SQLException;

}
