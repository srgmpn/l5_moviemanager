/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.dates.json;

import com.srg.core.Mediator;
import com.srg.pojo.Actor;
import com.srg.pojo.Category;
import com.srg.pojo.Director;
import com.srg.pojo.Movie;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author administrator
 */
public class UrlParser {
    
    private Mediator med;
    final static Logger logger = Logger.getLogger(UrlParser.class);
    
    public UrlParser(Mediator med){
        this.med = med;
    }
    
    /**
     * parse url - json format
     * @param urlJsonFormat 
     */
    public void parseUrlJsonFormat(String urlJsonFormat){
        
        Movie movie = movie = new Movie();
        Director director  = new Director();
        List<Actor> actors = new ArrayList<>();
        List<Category> categories = new ArrayList<>();

        try {       
           JSONParser jsonParser = new JSONParser();
           JSONObject jsonObject = (JSONObject) jsonParser.parse(urlJsonFormat);
           
           movie.setTitle((String)jsonObject.get("Title"));
           movie.setImbd((String)jsonObject.get("imdbID"));
           movie.setYear((String)jsonObject.get("Year"));
           movie.setRuntime((String)jsonObject.get("Runtime"));
           movie.setLang((String)jsonObject.get("Language"));
           director.setName((String)jsonObject.get("Director"));
           
           String l1 = (String)jsonObject.get("Actors");
           String [] acts = l1.split(",");
           
           for (String a : acts){
                Actor act = new Actor();
                act.setName(a.trim());
                actors.add(act);
            }
           
           String l2 = (String)jsonObject.get("Genre");    
           String [] cts = l2.split(",");
           
            for (String c : cts){
                Category ct = new Category();
                ct.setCategory(c.trim());
                categories.add(ct);
            }
            
             movie.setJsonUrl(jsonObject.toString());
             
            logger.info(" parseUrlJsonFormat ");
            
            med.setParsedData(movie, director, categories, actors);
            
        } catch (ParseException ex) {
            logger.error("parseUrlJsonFormat " + ex.toString());
        }
    }
    
    /**
     * parse json string jsonUrl
     * @param jsonUrl
     * @return - List with parsed elements
     */
    public List getFullMovieInformation(String jsonUrl){
            List<String> fullInf = new ArrayList();
            
        try {
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(jsonUrl);
            //get full information of movie
            fullInf.add((String)jsonObject.get("Title"));
            fullInf.add((String)jsonObject.get("Year"));
            fullInf.add((String)jsonObject.get("Rated"));
            fullInf.add((String)jsonObject.get("Released"));
            fullInf.add((String)jsonObject.get("Runtime"));
            fullInf.add((String)jsonObject.get("Genre"));
            fullInf.add((String)jsonObject.get("Director"));
            fullInf.add((String)jsonObject.get("Writer"));
            fullInf.add((String)jsonObject.get("Actors"));
            fullInf.add((String)jsonObject.get("Plot"));
            fullInf.add((String)jsonObject.get("Language"));
            fullInf.add((String)jsonObject.get("Country"));
            fullInf.add((String)jsonObject.get("Awards"));
            fullInf.add((String)jsonObject.get("Metascore"));
            fullInf.add((String)jsonObject.get("imdbRating"));
            fullInf.add((String)jsonObject.get("imdbVotes"));
            fullInf.add((String)jsonObject.get("imdbID"));
            fullInf.add((String)jsonObject.get("Type"));
            
        } catch (ParseException ex) {
            logger.error("getFullMovieInformation " + ex);
        }
        return fullInf;
        
    } 
}