/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.core;

import com.srg.buttons.AddButton;
import com.srg.buttons.DeleteButton;
import com.srg.buttons.ExportPDFButton;
import com.srg.buttons.ExportXMLButton;
import com.srg.components.MoviesJTable;
import com.srg.models.JTableModel;
import com.srg.models.MapTreeModel;
import javax.swing.JFrame;
import static com.srg.util.Constants.*;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextPane;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import org.apache.log4j.Logger;

/**
 *
 * @author administrator
 */
public class GuiMain extends JFrame{
    
    private AddButton addBtn;
    private DeleteButton delBtn;
    private ExportPDFButton pdfExportBtn;
    private ExportXMLButton xmlExportBtn;
    
    private MoviesJTable jTable;
    private JTableModel tmodel;
    
    private JTextPane jtextpane;
    
    private JTree jTree;
    private MapTreeModel model;
    
    private JPanel cards;
    
    private Mediator med;
    
    final static Logger logger = Logger.getLogger(GuiMain.class);
    
    public GuiMain(){
        super(FRAME_NAME);
        this.initUI();
    }
    /**
     * main function of class
     */
    private void initUI(){
        logger.info("initUI");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(new Dimension(HEIGHT, WIDTH));
        setLocationRelativeTo(null);
        getContentPane().setLayout(new BorderLayout());
        //get mediator
        med = Mediator.getInstance();
        
        createComponents();
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
        getContentPane().add(mainPane());
        
        //set frame border
        getRootPane().setBorder(BorderFactory.createMatteBorder(10, 10, 10, 10, Color.LIGHT_GRAY));
        pack();
        setVisible(true);
    }
    
    /**
     * create all components of frame
     */
    private void createComponents(){
        
        ActionListener btnListener = getActionListener();
        
        addBtn = new AddButton(ADD, new ImageIcon(GuiMain.class.getResource(IMAGE_ADD)), btnListener, med, this);
        delBtn = new DeleteButton(DELETE, new ImageIcon(GuiMain.class.getResource(IMAGE_DELETE)), btnListener, med, this);
        pdfExportBtn = new ExportPDFButton(EXPORT_PDF, new ImageIcon(GuiMain.class.getResource(IMAGE_EXPORT)), btnListener, med);
        xmlExportBtn = new ExportXMLButton(EXPORT_XML, new ImageIcon(GuiMain.class.getResource(IMAGE_EXPORT)), btnListener, med);
        
        //create treemodel       
        model = new MapTreeModel();
        
        //redirect to mediator
        med.setJTreeModel(model);
        

        jTree = new JTree(model);
        jTree.addTreeSelectionListener(getTreeListener());
        
        tmodel = new JTableModel();
        //tmodel.
        jTable = new MoviesJTable(tmodel);
        jTable.setRowSelectionAllowed(true);
        jTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jTable.addMouseListener(getTableMouseListener());
        
        jtextpane = new JTextPane();
        jtextpane.setContentType("text/html");
    }
    
    /**
     * put all panels in main panel
     * @return JSplitPane
     */
    private JSplitPane mainPane(){
        
        cards = new JPanel(new CardLayout());
        cards.add(getInformationPanel(), "1");
        cards.add(getJTextPane(), "2");
        
        JSplitPane l = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, getJTreePanel(), cards);
        
        l.setDividerLocation(300);
        l.setOneTouchExpandable(true);
        
        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, getButtonsPanel(), l);        
        return splitPane;
    }
    
    /**
     * put all button in panel
     * @return 
     */
    private JPanel getButtonsPanel(){
        
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        panel.add(addBtn);
        panel.add(delBtn);
        panel.add(pdfExportBtn);
        panel.add(xmlExportBtn);
        
        JPanel pan = new JPanel();
        pan.setLayout(new BoxLayout(pan, BoxLayout.LINE_AXIS));
        pan.add(Box.createRigidArea(new Dimension(250, 20)));
        pan.add(panel);
        pan.add(Box.createRigidArea(new Dimension(250, 20)));
        
        return pan;
    }
    
    /**
     * jTree panel
     * @return panel
     */
    private JPanel getJTreePanel(){
        
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        jTree.putClientProperty("JTree.lineStyle", "Horizontal");
        panel.add(new JScrollPane(jTree));
        return panel;
    }
    
    /**
     * Jtable panel
     * @return panel
     */
    private JPanel getInformationPanel(){
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        panel.add(new JScrollPane(jTable), BorderLayout.CENTER);
        
        return panel;
    }
    
    /**
     * jTextPane panel
     * @return panel
     */
    private JPanel getJTextPane(){
        
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        JScrollPane pane = new JScrollPane();
        pane.getViewport().add(jtextpane);
        panel.add(pane, BorderLayout.CENTER);
        
        return panel;
    }
    
    /**
     * get listener for jTree
     * @return TreeSelectionListener
     */
    private TreeSelectionListener getTreeListener(){
        return new TreeSelectionListener() {

            @Override
            public void valueChanged(TreeSelectionEvent e) {
                CardLayout cl = (CardLayout)(cards.getLayout());
                Object action = (Object)e.getPath().getLastPathComponent();
                
                if (med.isMovie(action)){
                    delBtn.setEnabled(true);
                    //redirect to mediator to set dates in jtextpane
                    med.setPageJTextPane(jtextpane, action);
                    //sh0w jtextpane palen
                    cl.show(cards, "2");
                    
                } else {
                    delBtn.setEnabled(false);
                    //redirect to mediator to set dates in table model
                    med.setTableModel(tmodel, action);
                    // card layout show table panel
                    cl.show(cards, "1");
                }
            }
        };
    }
    
    /**
     * get listener for Buttons
     * @return ActionListener
     */
    private ActionListener getActionListener(){
        return new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                ((Command)e.getSource()).execute();
            }
        };
    }

    /**
     * create mouse listener for jTabel
     * @return
     */
    private MouseListener getTableMouseListener(){
        return new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                delBtn.setEnabled(true);
                med.setSelectedRow(jTable.getSelectedRow());
            }
        };
    }
}
