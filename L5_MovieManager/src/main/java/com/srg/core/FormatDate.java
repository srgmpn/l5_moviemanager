/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.core;

import com.srg.pojo.Movie;
import java.util.List;

/**
 *
 * @author administrator
 */
public class FormatDate {
    
    public static String getFormatMovie(List movie){
        
        StringBuilder html = new StringBuilder();
        html.append("<H2 color=\"RED\">  Movie Full Information  </h2>");
        html.append("<H4><b color=\"BLUE\">Title</b> : " + movie.get(0) + "</h4>");
        html.append("<H4><b color=\"BLUE\">Year</b> : " + movie.get(1) + "</h4>");
        html.append("<H4><b color=\"BLUE\">Rated</b> : " + movie.get(2) + "</h4>");
        html.append("<H4><b color=\"BLUE\">Released</b> : " + movie.get(3) + "</h4>");
        html.append("<H4><b color=\"BLUE\">Runtime</b> : " + movie.get(4) + "</h4>");
        html.append("<H4><b color=\"BLUE\">Genre</b> : " + movie.get(5) + "</h4>");
        html.append("<H4><b color=\"BLUE\">Director</b> : " + movie.get(6) + "</h4>");
        html.append("<H4><b color=\"BLUE\">Writer</b> : " + movie.get(7) + "</h4>");
        html.append("<H4><b color=\"BLUE\">Actors</b> : " + movie.get(8) + "</h4>");
        html.append("<H4><b color=\"BLUE\">Plot</b> : " + movie.get(9) + "</h4>");
        html.append("<H4><b color=\"BLUE\">Languages</b> : " + movie.get(10) + "</h4>");
        html.append("<H4><b color=\"BLUE\">Country</b> : " + movie.get(11) + "</h4>");
        html.append("<H4><b color=\"BLUE\">Awards</b> : " + movie.get(12) + "</h4>");
        html.append("<H4><b color=\"BLUE\">Metascore</b> : " + movie.get(13) + "</h4>");
        html.append("<H4><b color=\"BLUE\">Imdb Rating</b> : " + movie.get(14) + "</h4>");
        html.append("<H4><b color=\"BLUE\">Imdb Votes</b> : " + movie.get(15) + "</h4>");
        html.append("<H4><b color=\"BLUE\">Id Imdb</b> : " + movie.get(16) + "</h4>");
        html.append("<H4><b color=\"BLUE\">Type</b> : " + movie.get(17) + "</h4>");
 
        return html.toString();
    }
    
    public static String getCantFindMessage(){
        return "<H2 color=\"RED\">  Movie Full Information  </h2>"
                + "<H2 color=\"BLUE\">  Can't find !  </h2>";
    }
}
