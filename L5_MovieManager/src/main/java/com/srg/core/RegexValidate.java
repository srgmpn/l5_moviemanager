/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.core;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author administrator
 */
public class RegexValidate {
    private Pattern pattern;
    private Matcher matcher;
    private static final String IBDM_PATTERN = "^(tt\\d{7})$";
    private static final String YEAR_PATTERN = "^(19|20)\\d{2}$";
    
    public RegexValidate() {

    }
    
    public void setPatternString(String s){
        if (s.equals("idIMBD")){
            pattern = Pattern.compile(IBDM_PATTERN);
            
        } else {
            pattern = Pattern.compile(YEAR_PATTERN);
        }
    }
        /**
     * Validate hex with regular expression
     *
     * @param hex     *            hex for validation
     * @return true valid hex, false invalid hex
     */
    public boolean validate(final String hex, String type) {
        
        setPatternString(type);
        matcher = pattern.matcher(hex);
        
        return matcher.matches();
    }
    
}
