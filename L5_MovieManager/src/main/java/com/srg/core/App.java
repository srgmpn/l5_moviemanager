/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.core;

import com.srg.dao.impl.DirectorDao;
import com.srg.pojo.Director;
import java.sql.SQLException;
import java.util.List;
import javax.swing.SwingUtilities;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author administrator
 */
public class App {
    
    final static Logger logger = Logger.getLogger(App.class);
    
    public static void main(String[] args) {
        
        logger.info(" ----- Start App  --------- ");
        BasicConfigurator.configure();
        
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new GuiMain();
            }
        });
    }
}