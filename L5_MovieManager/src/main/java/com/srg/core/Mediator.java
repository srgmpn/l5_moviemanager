/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.core;

import com.srg.data.DataManager;
import com.srg.dates.json.UrlParser;
import com.srg.export.JasperRaportExportPdf;
import com.srg.export.StAXCreateXML;
import com.srg.models.JTableModel;
import com.srg.models.MapTreeModel;
import com.srg.pojo.Actor;
import com.srg.pojo.Category;
import com.srg.pojo.Director;
import com.srg.pojo.Movie;
import static com.srg.util.Constants.*;
import com.srg.restclient.IMDBHttpClient;
import com.srg.restclient.UrlBuild;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JTextPane;
import javax.xml.stream.XMLStreamException;
import net.sf.jasperreports.engine.JRException;
import org.apache.log4j.Logger;



/**
 *
 * @author administrator
 */
public class Mediator {
    
    private static Mediator INSTANCE;
    
    private Map modelMap;
    private String []args;
    private JTableModel tmodel;
    
    private UrlBuild urlBuild;
    private UrlParser parser;
    
    private DataManager dataManager;

    private Movie newMovie, selectedMovie;
    private Director newDirector;
    private List<Category> newCategories;
    private List<Actor> newActors;
    
    

    final static Logger logger = Logger.getLogger(Mediator.class);
    
    private Mediator(){
        
        args = new String[3];
        
        modelMap = new HashMap();
        List rootList = new ArrayList();
        rootList.add(ACTOR);       
        rootList.add(CATEGORY);      
        rootList.add(MOVIE);
        rootList.add(DIRECTOR);
        modelMap.put(CATALOG, rootList);
        
        urlBuild = new UrlBuild(this);
        parser = new UrlParser(this);
     
        dataManager = new DataManager(this);
    }
    
    /**
     * singleton object
     * @return Instance
     */
    public static Mediator getInstance(){
        
        if (INSTANCE == null){
            INSTANCE = new Mediator();
        }
        
        return INSTANCE;
    } 
 
    /**
     * set model - JTree this has map model
     * and set root
     * call updateDates for update all data 
     * is database
     * @param model 
     */
    public void setJTreeModel(MapTreeModel model){
        logger.info("setJTreeModel");
        model.setMap(modelMap);
        model.setRoot(CATALOG);
        dataManager.loadDates();
    }
    
    /**
     *show movie in depend of selected node
     * if is root key - set all movies
     * else show in depend of key
     * @param model - JTable model
     * @param action - selected JTree action
     */
    public void setTableModel(JTableModel model, Object action){
        logger.info("setTableModel");
        
        this.tmodel = model;
        
        if (action.equals(ACTOR) || action.equals(DIRECTOR) || 
            action.equals(MOVIE) || action.equals(CATEGORY) || 
                action.equals(CATALOG))
            model.add(dataManager.getMovieLocalMap().values());
        
        else{
            
            List list = new ArrayList();
            for (Object key : ((List)modelMap.get(action))){
                list.add(dataManager.getMovieLocalMap().get(key));
            }
            model.add(list);
        }
    }
    
    /**
     * get full movie information in format json
     * parse url and get string list with information
     * set pane text - in  html format
     * @param pane - textpane
     * @param action - selected title movie
     */
    public void setPageJTextPane(JTextPane pane, Object action){
        logger.info("setPageJTextPane");
        
        Movie m = (Movie) dataManager.getMovieLocalMap().get(action);
        selectedMovie = m;
        List fullInformation = parser.getFullMovieInformation(m.getJsonUrl());
        
        if (fullInformation.size() > 0)
            pane.setText(FormatDate.getFormatMovie(fullInformation));
        else {
            pane.setText(FormatDate.getCantFindMessage());
        }
    }
    
    /**
     * client completed fields
     * @param imdb - imdb
     * @param title - title movie
     * @param year  - year production
     */
    public void setDialogDates(String imdb, String title, String year){
        args[0] = imdb;
        args[1] = title;
        args[2] = year;
    }
    
    /**
     * validate form dates
     * @return true if is valid, else false
     */
    public boolean validateDialogDates(){
        logger.info(" validateDialogDates ");
        return urlBuild.validate();
    }
    
    /**
     * get parser object
     * build url with client dates, and get it
     * sendRequest to imdb and get response(in format json)
     * verif in is valid dates
     * parse result, and set new objects
     * insert data in DB
     * @return - true if dates was been saved, else false
     */
    public boolean dataInsert(){
        logger.info(" dataInsert ");
        
        parser = new UrlParser(this);
        
        String url = urlBuild.getUrl();
        
        String result = sendRequest(url);
        
        if (result.isEmpty() || result.length() < 510)
            return false;
        
        parser.parseUrlJsonFormat(result);
        
        try {
            dataManager.insertDates();
        } catch (SQLException ex) {
            logger.error( " dateInsert " + ex);
            return false;
        }
        
        return true;
    }
    
    /**
     * delete selected movie is table 
     * or is JTree
     * @return true if is deleted, else false
     */
    public boolean deleteMovie(){
        
        logger.info("deleteMovie");
        try {
            dataManager.deleteMovie(selectedMovie);
        } catch (SQLException ex) {
            logger.error("deleteMovie " + ex);
            return false;
        }
        
        return true;
    }
    
    /**
     * create a object IMDBHttpClient and call sendRequest
     * and get this request of type String in format Json
     * @param url - url to resource
     * @return - response of request
     */
    public String sendRequest(String url){
        
        String response = null;
        
        try {
            response = new IMDBHttpClient().sendRequest(url);
        } catch (IOException ex) {
            logger.error(" sendRequest " + ex);
        }
        
        return response;
    }
    
    /**
     * parsed data is response
     * @param movie - movie dates
     * @param director - direcor dates
     * @param categories - list categories
     * @param actors - list actors
     */
    public void setParsedData(Movie movie, Director director, List<Category> categories, List<Actor> actors){
        this.newActors = actors;
        this.newMovie = movie;
        this.newDirector = director;
        this.newCategories = categories;
    }
    
    /**
     * verif if is selected movie node in JTree
     * vrif in local map
     * @param node - name of selected movie 
     * @return - true if is movie, else false
     */
    public boolean isMovie(Object node){
        return dataManager.getMovieLocalMap().containsKey(node.toString());
    }
    
    /**
     * get selected value of Jtable model
     * @param row - row number
     */
    public void setSelectedRow(int row){
        selectedMovie = (Movie) tmodel.getRowValue(row);
    }
    
    /**
     * export all movies in  PDF foramt file,
     * use for pdf generation jasperraports
     * start in new Thread
     */
    public void exportPdf(){
        
        new Thread( new Runnable() {
            @Override
            public void run() {
                    try {
                        JasperRaportExportPdf.exportPdf(dataManager.getMovieLocalMap().values());
                    } catch (FileNotFoundException ex) {
                        logger.error("exportPdf " + ex);
                    } catch (JRException ex) {
                        logger.error("exportPdf " + ex);
                    }
            }
        }).start();
    }
    
    /**
     * export all movies in xml formar 
     * use a stax for build xml format
     * start in new Thread
     */
    public void exportXML(){
       
        new Thread( new Runnable() {
            @Override
            public void run() {
                try {
                    StAXCreateXML.exportXML(dataManager.getMovieLocalMap().values());
                } catch (XMLStreamException ex) {
                   logger.error("exportXML " + ex);
                } catch (IOException ex) {
                    logger.error("exportXML " + ex);
                }
            }
        }).start();       
        
    }
    
    /**
     * get dates is form
     * @return 
     */
    public String[] getArgs(){
        return args;
    }
    
    /**
     * get map modelof JTree
     * @return map
     */
    public Map getMap(){
        return modelMap;
    }
    
    /**
     * get new added actors
     * @return - Actor list objects 
     */
    public List<Actor> getNewActor(){
        return newActors;
    }
    
    /**
     * get new added movie
     * @return - Movie object
     */
    public Movie getNewMovie(){
        return newMovie;
    }
    
    /**
     * get new  added categories
     * @return - Categories list objects
     */
    public List<Category> getNewCategories(){
        return newCategories;
    }
    
    /**
     * get new added director
     * @return  - Director object
     */
    public Director getNewDirector(){
        return newDirector;
    }
}
