/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.restclient;

import com.srg.core.Mediator;
import com.srg.core.RegexValidate;
import org.apache.log4j.Logger;

/**
 *
 * @author administrator
 */
public class UrlBuild {
    
    private Mediator med;
    private String [] args;
    private RegexValidate regex;
    final static Logger logger = Logger.getLogger(UrlBuild.class);
    
    public UrlBuild(Mediator med){
        this.med = med;
        args = med.getArgs();
        regex = new RegexValidate();
    }
    
    /**
     * validate form dates
     * @return true if is valid, else false
     */
    
    public boolean validate(){
        logger.info(" validate ");
        //imbd and title is false error
        if (args[0].isEmpty() && args[1].isEmpty())
            return false;
        
        if (!args[0].isEmpty() && regex.validate(args[0], "idIMBD")){
            return true;
        }
        
        if (!args[1].isEmpty()){
            args[2] = (regex.validate(args[2], "year") ? args[2] : "");             
            return true;
        }
        return false;        
    }
    
    /**
     * get url with dates
     * @return url
     */
    public String getUrl(){
        logger.info(" getUrl ");
        String url = null;
        
        if (!args[0].isEmpty()){
            url = "http://www.omdbapi.com/?i=" + args[0] + "&plot=short&r=json";
            return url;
        }
        url = "http://www.omdbapi.com/?t=" + parseTitle() + "&y="+ args[2] +"&plot=short&r=json";
        
        return url;
    }
    
    /**
     * parse title and set format
     * @return titleUrl
     */
    private String parseTitle(){
        
        String [] words = args[1].split(" ");
        StringBuilder urlTitle = new StringBuilder();
        int wordL = words.length-1;
        
        for (int i = 0; i < wordL; ++i){
            urlTitle.append(words[i]).append("+");
        }
        urlTitle.append(words[wordL]);
        
        return urlTitle.toString();
    }
}
