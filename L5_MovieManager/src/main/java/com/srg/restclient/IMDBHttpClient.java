/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.restclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

/**
 *
 * @author administrator
 */
public class IMDBHttpClient {
    final static Logger logger = Logger.getLogger(IMDBHttpClient.class);
    
    public String sendRequest(String url) throws IOException{
        logger.info("request");
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);
        
        HttpResponse response = client.execute(request);

        BufferedReader rd = new BufferedReader (new InputStreamReader(response.getEntity().getContent()));
        String line = "";
        String rez = "";

        while ((line = rd.readLine()) != null) {
            rez += line;    
        }
        return rez;
    }
}

