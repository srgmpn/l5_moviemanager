/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.export;

import com.srg.pojo.Movie;
import static com.srg.util.Constants.EXPORT_PATH_FILE_NAME_XML;
import static com.srg.util.Constants.EXPORT_PATH_FOLDER;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

/**
 *
 * @author administrator
 */
public class StAXCreateXML {
    /**
     * build a xml file with movies, use
     * stax 
     * @param movies list with movies
     * @throws XMLStreamException
     * @throws IOException 
     */
    public static void exportXML(Collection movies) throws XMLStreamException, IOException{
                
        File file = new File(EXPORT_PATH_FOLDER);
        if (!file.exists()){
            file.mkdir();
        }
        
        XMLOutputFactory xMLOutputFactory = XMLOutputFactory.newInstance();	
        XMLStreamWriter xMLStreamWriter = xMLOutputFactory.createXMLStreamWriter(new FileWriter(EXPORT_PATH_FILE_NAME_XML));        
        
        xMLStreamWriter.writeStartDocument();
        xMLStreamWriter.writeStartElement("movies");
        
        for (Object m : movies){
            
            Movie movie = (Movie)m;
            
            xMLStreamWriter.writeStartElement("movie");
            
            xMLStreamWriter.writeStartElement("title");
            xMLStreamWriter.writeCharacters(movie.getTitle());
            xMLStreamWriter.writeEndElement();
            
            xMLStreamWriter.writeStartElement("year");
            xMLStreamWriter.writeCharacters(movie.getYear());
            xMLStreamWriter.writeEndElement();
            
            xMLStreamWriter.writeStartElement("lang");
            xMLStreamWriter.writeCharacters(movie.getLang());
            xMLStreamWriter.writeEndElement();
            
            xMLStreamWriter.writeStartElement("runtime");
            xMLStreamWriter.writeCharacters(movie.getRuntime());
            xMLStreamWriter.writeEndElement();  
            
            xMLStreamWriter.writeStartElement("imdb");
            xMLStreamWriter.writeCharacters(movie.getImbd());
            xMLStreamWriter.writeEndElement();
            
            xMLStreamWriter.writeEndElement();
        }
        xMLStreamWriter.writeEndDocument();
        xMLStreamWriter.flush();
        xMLStreamWriter.close();
       
    }
}
