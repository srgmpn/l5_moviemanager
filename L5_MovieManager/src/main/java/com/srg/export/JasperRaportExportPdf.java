/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.export;

import com.srg.pojo.Movie;
import static com.srg.util.Constants.EXPORT_PATH_FILE_NAME_PDF;
import static com.srg.util.Constants.EXPORT_PATH_FOLDER;
import static com.srg.util.Constants.FILE_PATH_REPORT_TEMPLATE;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 *
 * @author administrator
 */
public class JasperRaportExportPdf {
    
   /**
    * create a pdf report with movies using 
    * JasperReports 
    * @param persons
    * @throws FileNotFoundException
    * @throws JRException 
    */    
    public static void exportPdf(Collection<Movie> persons) throws FileNotFoundException, JRException{
        
        InputStream inputStream = new FileInputStream (JasperRaportExportPdf.class.getClassLoader().getResource(FILE_PATH_REPORT_TEMPLATE).getFile());
        JRBeanCollectionDataSource beanColDataSource = new
                    JRBeanCollectionDataSource(persons);
        
        Map parameters = new HashMap();
         
        JasperDesign jasperDesign = JRXmlLoader.load(inputStream);
        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, beanColDataSource);
        
                
        File file = new File(EXPORT_PATH_FOLDER);
        if (!file.exists()){
            file.mkdir();
        }
        
        JasperExportManager.exportReportToPdfFile(jasperPrint, EXPORT_PATH_FILE_NAME_PDF); 

    }
}
