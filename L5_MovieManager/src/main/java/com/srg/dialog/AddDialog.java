/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.dialog;

import com.srg.buttons.AddButtonDialog;
import com.srg.buttons.CloseButtonDialog;
import com.srg.core.Command;
import com.srg.core.Mediator;
import static com.srg.util.Constants.*;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;

/**
 *
 * @author administrator
 */
public class AddDialog extends JDialog implements ItemListener{   
   
    private JLabel message, disp, title, year;
    private JComboBox<String> box;
    private AddButtonDialog addBtn;
    private CloseButtonDialog closeBtn;
    private Mediator med;
    private JTextField ibdm, title_text, year_text;
    private JFrame parent;
    
    private JPanel cards;

    public AddDialog(JFrame parent, String title, Mediator med){
        super(parent, title, true);
        this.med = med;
        if (parent != null) {
            Dimension parentSize = parent.getSize(); 
            Point p = parent.getLocation(); 
            setLocation(p.x + parentSize.width / 4, p.y + parentSize.height / 4);
            this.parent = parent;
        }
        
        initUI();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack(); 
        setVisible(true);  
    }
    
    private void initUI(){
              
        cards = new JPanel(new CardLayout());
        cards.add(getPanelC1(), TITLE_YEAR);
        cards.add(getPanelC2(), ID_IBDM);
        
        JSplitPane slpit1 = new JSplitPane(JSplitPane.VERTICAL_SPLIT, getComboboxPanel(), cards);
        JSplitPane slpit2 = new JSplitPane(JSplitPane.VERTICAL_SPLIT, slpit1, getButtonJP());
        
        add(slpit2);
        
    }
    
    private JPanel getComboboxPanel(){
        String comboBoxItems[] = { TITLE_YEAR, ID_IBDM };
        JPanel comboBoxPane = new JPanel();
        comboBoxPane.setLayout(new BoxLayout(comboBoxPane, BoxLayout.PAGE_AXIS));
        box = new JComboBox<>(comboBoxItems);
        box.setEditable(false);
        message = new JLabel("Select method to introduce movie (* - required field ) ");
        box.addItemListener(this);
        comboBoxPane.add(message);
        comboBoxPane.add(box); 
        return comboBoxPane;
    }
    
    private JPanel getButtonJP(){
        
        ActionListener listener = getActionListener();
        
        JPanel buttonPanel = new JPanel(new FlowLayout());     
        addBtn = new AddButtonDialog(ADD_MOVIE,
                new ImageIcon(AddDialog.class.getResource(IMAGE_ADDDIALOG)), listener, med, this); 
        closeBtn = new CloseButtonDialog(CLOSE,
                new ImageIcon(AddDialog.class.getResource(IMAGE_CLOSEDIALOG)), listener, this);
        buttonPanel.add(addBtn);
        buttonPanel.add(closeBtn);
        return buttonPanel;
    }
    
    private JPanel getPanelC1(){
  
        title = new JLabel("Set Movie Title  : *");
        title_text = new JTextField(20);
        year = new JLabel("Production year : ");
        year_text = new JTextField(10);
        
        JPanel card = new JPanel();
        card.setLayout(new BoxLayout(card, BoxLayout.PAGE_AXIS));
        
        JPanel pan1 = new JPanel(new FlowLayout(5, 5, 5));
        pan1.add(title);
        pan1.add(title_text);
        
        JPanel pan2 = new JPanel(new FlowLayout(5, 5, 5));
        pan2.add(year);
        pan2.add(year_text);
        
        card.add(pan1);
        card.add(pan2);
        
        return card;
    }
    
    private JPanel getPanelC2(){
  
        disp = new JLabel("Set id ibmd : *");
        ibdm = new JTextField(10);

        JPanel card = new JPanel();
        card.setLayout(new BoxLayout(card, BoxLayout.PAGE_AXIS));
        
        JPanel pan = new JPanel(new FlowLayout(5, 5, 5));
        pan.add(disp);
        pan.add(ibdm);
 
        card.add(pan);
        
        return card;
    }
    
    @Override
    public void itemStateChanged(ItemEvent e) {
        setNullFields();
        CardLayout cl = (CardLayout)(cards.getLayout());
        cl.show(cards, (String)e.getItem());     
    }
    
    public ActionListener getActionListener(){
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                med.setDialogDates(ibdm.getText(), title_text.getText(), year_text.getText());
                
                ((Command)e.getSource()).execute();
                //setNullFields();
            }
        };
    }
    
    private void setNullFields(){
        ibdm.setText("");
        title_text.setText("");
        year_text.setText("");
    }
}
