/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.components;

import com.srg.models.JTableModel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author administrator
 */
public class MoviesJTable extends JTable{
    
    public MoviesJTable(JTableModel model){
        super(model);
    }
    
    @Override
    public void changeSelection(int rowIndex, int columnIndex,
                                boolean toggle, boolean extend) {
        super.changeSelection(rowIndex, columnIndex, true, false);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
}
