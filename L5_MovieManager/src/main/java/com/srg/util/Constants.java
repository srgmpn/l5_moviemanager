/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.util;

/**
 *
 * @author administrator
 */
public class Constants {
    
    public static final String FRAME_NAME = "Movie Catalog";
    public static final int HEIGHT = 1000;
    public static final int WIDTH = 700;
    public static final String FILE_PATH_REPORT_TEMPLATE = "reports/report1.jrxml"; 
    public static final String EXPORT_PATH_FILE_NAME_XML = "export/catalog_Movies.xml";
    public static final String EXPORT_PATH_FILE_NAME_PDF = "export/Catalog_Movies.pdf";
    public static final String EXPORT_PATH_FOLDER = "export"; 
    public static final String ADD_MOVIE = "Add";
    public static final String ADD = "Add Movie";
    public static final String DELETE = "Delete Movie";
    public static final String CLOSE = "Close";
    public static final String EXPORT_PDF = "Export pdf";
    public static final String EXPORT_XML = "Export xml";
    public final static String ID_IBDM = "Movie Id IMDB";
    public final static String TITLE_YEAR = "Movie Title and year";
    public static final String IMAGE_ADDDIALOG = "/images/adddialog.png";
    public static final String IMAGE_CLOSEDIALOG = "/images/close.png";
    public static final String IMAGE_ADD = "/images/add.png";
    public static final String IMAGE_DELETE = "/images/delete.png";
    public static final String IMAGE_EXPORT = "/images/export.png";
    public static final String CATALOG = "Catalog";
    public static final String CATEGORY = "Category";
    public static final String ACTOR = "Actor";
    public static final String DIRECTOR = "Director";
    public static final String MOVIE = "Movie";
    public static final String [] NAME_COL      = {"Title", "Year", "Language", "Runtime", "imdbID"};
    //db props
    public static final String DRIVER = "com.mysql.jdbc.Driver"; 
    public static final String URL = "jdbc:mysql://";
    public static final String DBNAME = "catalog_movies";
    public static final String HOST = "localhost";
    public static final String PORT = "3306";
    public static final String USERNAME = "root";
    public static final String PASSWORD = "123";

}
