/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.data;

import com.srg.loaddata.LoadDatesMovies;
import com.srg.loaddata.LoadDatesCategories;
import com.srg.loaddata.LoadDatesActors;
import com.srg.loaddata.LoadDatesDirectors;
import com.srg.core.Mediator;
import com.srg.dao.Dao;
import com.srg.dao.DaoID;
import com.srg.dao.impl.DaoFactory;
import com.srg.dao.impl.DaoIDFactory;
import com.srg.pojo.Actor;
import com.srg.pojo.Category;
import com.srg.pojo.Director;
import com.srg.pojo.Movie;
import com.srg.pojo.MovieActor;
import com.srg.pojo.MovieCategory;
import com.srg.util.CommandsDao;
import com.srg.util.CommandsDaoID;
import static com.srg.util.Constants.ACTOR;
import static com.srg.util.Constants.CATEGORY;
import static com.srg.util.Constants.DIRECTOR;
import static com.srg.util.Constants.MOVIE;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;


/**
 *
 * @author administrator
 */
public class DataManager {
    
    private Mediator med;

    private Map<String, Actor> actorMap;
    private Map<String, Movie> movieMap;
    private Map<String, Category> categoryMap;
    private Map<String, Director> directorMap;
    
    private DaoFactory factory;
    private DaoIDFactory factoryID;
    private Movie newMovie;
    private Director newDirector;
    
    final static Logger logger = Logger.getLogger(DataManager.class);
    
    public DataManager(Mediator med){
        this.med = med;

        directorMap = new HashMap<>();
        movieMap = new HashMap<>();
        categoryMap = new HashMap<>();
        actorMap = new HashMap<>();
    }
    
    public Map getMovieLocalMap(){
        return movieMap;
    }
    
    /**
     * update dates in JTree map - connect to
     * Database and select all dates
     */
    public void loadDates(){
        logger.info(" loadDates ");
        new LoadDatesActors(med.getMap(), actorMap).load();
        new LoadDatesMovies(med.getMap(), movieMap).load();
        new LoadDatesDirectors(med.getMap(), directorMap).load();
        new LoadDatesCategories(med.getMap(), categoryMap).load();  
    }
    
    public void deleteMovie(Movie m) throws SQLException{
        
        logger.info(" delete Movie ");
        
        factory = DaoFactory.getInstanceDaoFactory();
        factoryID = DaoIDFactory.getInstanceDaoFactory();
        
        DaoID m_cDao = factoryID.getDaoID(CommandsDaoID.MOVIE_CATEGORY);
        DaoID m_aDao = factoryID.getDaoID(CommandsDaoID.MOVIE_ACTOR);
        Dao movieDao = factory.getDao(CommandsDao.MOVIE);
        
        MovieActor m_a = new MovieActor();
        m_a.setIdMovie(m.getId());
        
        MovieCategory m_c = new MovieCategory();
        m_c.setIdMovie(m.getId());
        
        //remove relation movie actor
        m_aDao.delete(m_a);
        //remove relation movie category
        m_cDao.delete(m_c);
        //remove movie
        movieDao.delete(m);
        //delete movie is all dates structures
        movieMap.remove(m.getTitle());
        ((List)med.getMap().get(MOVIE)).remove(m.getTitle());
        
        //remove movie is actors list
        for (Actor a : actorMap.values()){
            ((List)med.getMap().get(a.getName())).remove(m.getTitle());
        }
        
        //remove movie is categories list
        for (Category c : categoryMap.values()){
            ((List)med.getMap().get(c.getCategory())).remove(m.getTitle());
        }
        
        //remove movie is directors list
        for (Director d : directorMap.values()){
            ((List)med.getMap().get(d.getName())).remove(m.getTitle());
        }
    }
    
    /**
     * insert new data in DB and update 
     * JTree model
     * @throws SQLException 
     */
    public void insertDates() throws SQLException{
        logger.info(" insertDates ");
        
        factory = DaoFactory.getInstanceDaoFactory();
        factoryID = DaoIDFactory.getInstanceDaoFactory();
        
        int idDir = insertDirector();
        insertMovie(idDir);
        insertCategory();
        insertActor();
    }
    
    /**
     * getNewDirector of mediator object;
     * insert new value in director verif if exist value in local map
     * then it isn't create new value, only is added new movie to 
     * director list, is return idDirector is DB - foregin key in movie
     * @return - id Director
     * @throws SQLException 
     */
    private int insertDirector() throws SQLException{
        logger.info(" insertDates(Director) ");
        
        Dao directorDao = factory.getDao(CommandsDao.DIRECTOR);  
        Director newDirector = med.getNewDirector();//get new Director

        if (!directorMap.containsKey(newDirector.getName())){ //verif if exist
            //insert new Director
            directorDao.insert(newDirector); 
            //get new director and DB id
            newDirector =(Director) directorDao.get(med.getNewDirector().getName());
            //insert in local map
            directorMap.put(newDirector.getName(), newDirector);
            //insert new Director and new movie in JTree
            insertNewValuesInJTreeModel(DIRECTOR, newDirector.getName(), med.getNewMovie().getTitle());

        } else {
            //get existing director is local map
            newDirector = directorMap.get(newDirector.getName());
            //add new movie to exisiting director
            ((List)med.getMap().get(newDirector.getName())).add(med.getNewMovie().getTitle());
        }
  
        return newDirector.getId();
    }
    
    /**
     * getNewMovie of mediator object;
     * insert new value in tabel Moveie, in local map and in JTree 
     * @param idDir - id Director
     * @throws SQLException 
     */
    private void insertMovie(int idDir) throws SQLException{
        logger.info(" insertDates(Movie) ");
        
        Dao movieDao = factory.getDao(CommandsDao.MOVIE);
        //get new movie
        newMovie = med.getNewMovie();
        //se Directory id
        newMovie.setIdDirector(idDir);
        //insert in DB
        movieDao.insert(newMovie);
        //get movie with DB id
        newMovie = (Movie) movieDao.get(med.getNewMovie().getImbd());
        //put in local map
        movieMap.put(newMovie.getTitle(), newMovie);
        //add new movie to List JTree
        ((List)med.getMap().get(MOVIE)).add(newMovie.getTitle());

    }
    
    /**
     * getNewCategoryList of mediator object;
     * Insert New categories in tabel Category, in local map and in JTree model
     * if not exist, and add new movie to category movie list
     * Insert Relation between Movie and Category 
     * @throws SQLException 
     */
    private void insertCategory() throws SQLException{
        logger.info(" insertDates(Category) ");
        
        Dao categoryDao = factory.getDao(CommandsDao.CATEGORY);
        DaoID m_cDao = factoryID.getDaoID(CommandsDaoID.MOVIE_CATEGORY);
        
         //get all new categories
        List<Category> categories = med.getNewCategories();
        //foreach categories verif if exist or is new
        for (Category c : categories){
            //verif if exist
            if (!categoryMap.containsKey(c.getCategory())){
                //insert new category
                categoryDao.insert(c);
                //get inserted c and id in DB
                c = (Category) categoryDao.get(c.getCategory());
                //insert in local map
                categoryMap.put(c.getCategory(), c);
                //insert new Category and new Movie
                insertNewValuesInJTreeModel(CATEGORY, c.getCategory(), newMovie.getTitle());

            } else {
                //get existing category is local map
                c = categoryMap.get(c.getCategory());
                //add new Movie to existing category
                ((List)med.getMap().get(c.getCategory())).add(newMovie.getTitle());
            }
            //create new relation in movie_category tabel
            MovieCategory m_c = new MovieCategory();
            //set newMovie id
            m_c.setIdMovie(newMovie.getId());
            //set category id
            m_c.setIdCategory(c.getId());
            //insert new value in DB
            m_cDao.insert(m_c);
        }        
    }
    
    /**
     * getNewActorList of mediator object;
     * Insert New actors in tabel Actors, in local map and in JTree model
     * if not exist, and add new movie to actor movie list
     * Insert Relation between Movie and Actor 
     * @throws SQLException 
     */
    private void insertActor() throws SQLException{
        logger.info(" insertDates(Actor) ");
        
        boolean nActor = false;
        Dao actorDao = factory.getDao(CommandsDao.ACTOR);
        DaoID m_aDao = factoryID.getDaoID(CommandsDaoID.MOVIE_ACTOR);
        //get all new actors
        List<Actor> newActors = med.getNewActor();
        //foreach actor verif if is new or exist
        for (Actor a : newActors){
            //if not exist add
            if (!actorMap.containsKey(a.getName())){ 
                //insert in DB
                actorDao.insert(a);
                //get actor with new DB id
                a = (Actor) actorDao.get(a.getName());
                //put in local map
                actorMap.put(a.getName(), a);
                //insert new Actor and new Movie
                insertNewValuesInJTreeModel(ACTOR, a.getName(), newMovie.getTitle());               
                
            } else {
                //get existing actor is local map
                a = actorMap.get(a.getName());
                 //add new Movie to existing actor
                ((List)med.getMap().get(a.getName())).add(newMovie.getTitle());               
            }
            //create new relation in movie_actor tabel
            MovieActor m_a = new MovieActor();
            //set actor id
            m_a.setIdActor(a.getId());
            //set new movie id
            m_a.setIdMovie(newMovie.getId());
            //insert new value
            m_aDao.insert(m_a);
        }        
    }
    
    private void insertNewValuesInJTreeModel(String key1, String key2, String value){
        //add in JTree list
        ((List)med.getMap().get(key1)).add(key2);
        //add new movie to new director JTree
        List list = new ArrayList();
        list.add(value);
        med.getMap().put(key2, list);       
    }
}
