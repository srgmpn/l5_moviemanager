/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.data;

import static com.srg.util.Constants.*;
import java.sql.Connection;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;

/**
 *
 * @author administrator
 */
public class DataSource {
    
    Connection connection = null;
    BasicDataSource bdSource = new BasicDataSource();
    final static Logger logger = Logger.getLogger(DataSource.class);
    
    public DataSource(){
        
        bdSource.setDriverClassName(DRIVER);
        bdSource.setUrl(URL + HOST + ":" + PORT + "/" + DBNAME);
        bdSource.setUsername(USERNAME);
        bdSource.setPassword(PASSWORD);
    }
    
    public Connection createConnection(){
        
        Connection con = null;
        try{

          if( connection != null ){
             logger.warn("Cant create a New Connection");
          }
          else{
            con = bdSource.getConnection();

          }
        }
        catch( Exception e ){
          logger.error("Error Occured " + e.toString());
        }
        return con;
    }
}
