/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.buttons;

import com.srg.core.Command;
import com.srg.core.Mediator;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import org.apache.log4j.Logger;

/**
 *
 * @author administrator
 */
public class AddButtonDialog extends JButton implements Command{

    private Mediator med;
    private JDialog frame;
    final static Logger logger = Logger.getLogger(AddButtonDialog.class);
    
    public AddButtonDialog(String msg, ImageIcon icon, JDialog frame){
        this(msg, icon, null, null, frame);
    }
    
    public AddButtonDialog(String msg, ImageIcon icon, ActionListener listener, Mediator med, JDialog frame){
        super(msg);
        setIcon(icon);
        addActionListener(listener);
        this.med = med;
        this.frame = frame;
        //setEnabled(false);
    }
    
    @Override
    public void execute() {
        
        logger.info(" execute ");
        if (!med.validateDialogDates()){  
            
            JOptionPane.showMessageDialog(frame,
                "Invalid dates !", "ERROR", JOptionPane.ERROR_MESSAGE);
        } else {
            
            if (med.dataInsert()){
            
                JOptionPane.showMessageDialog(frame, 
                    "Information has been successfully saved !",
                    "Confirmation", JOptionPane.OK_CANCEL_OPTION);
            } else {
                JOptionPane.showMessageDialog(frame, "Information not has saved !",
                        "Confirmation", JOptionPane.WARNING_MESSAGE);
            }
        }
    }
}
