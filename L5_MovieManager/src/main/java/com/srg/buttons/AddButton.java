/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.buttons;

import com.srg.core.Command;
import com.srg.core.Mediator;
import com.srg.dialog.AddDialog;
import static com.srg.util.Constants.ADD_MOVIE;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import org.apache.log4j.Logger;

/**
 *
 * @author administrator
 */
public class AddButton extends JButton implements Command{
    
    private Mediator med;
    private JFrame frame;
    final static Logger logger = Logger.getLogger(AddButton.class);
    
    public AddButton(String msg, ImageIcon icon, JFrame frame){
        this(msg, icon, null, null, frame);
    }
    
    public AddButton(String msg, ImageIcon icon, ActionListener listener, Mediator med, JFrame frame){
        super(msg);
        setIcon(icon);
        addActionListener(listener);
        this.med = med;
        this.frame = frame;
    }
    
    @Override
    public void execute() {
        logger.info(" execute ");
        new AddDialog(frame, ADD_MOVIE, med);
    }
}
