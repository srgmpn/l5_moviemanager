/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.buttons;

import com.srg.core.Command;
import com.srg.core.Mediator;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import org.apache.log4j.Logger;

/**
 *
 * @author administrator
 */
public class CloseButtonDialog  extends JButton implements Command{
    
    private JDialog dialog;
    final static Logger logger = Logger.getLogger(CloseButtonDialog.class);

    public CloseButtonDialog(String msg, ImageIcon icon){
        this(msg, icon, null, null);
    }
    
    public CloseButtonDialog(String msg, ImageIcon icon, ActionListener listener, JDialog dialog){
        super(msg);
        setIcon(icon);
        addActionListener(listener);
        this.dialog = dialog;
    }
    
    @Override
    public void execute() {
        logger.info(" execute ");
        dialog.dispose();
    }
}
