/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.buttons;

import com.srg.core.Command;
import com.srg.core.Mediator;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import org.apache.log4j.Logger;

/**
 *
 * @author administrator
 */
public class ExportXMLButton extends JButton implements Command{
    
    private Mediator med;
    final static Logger logger = Logger.getLogger(ExportXMLButton.class);
    
    public ExportXMLButton(String msg, ImageIcon icon){
        this(msg, icon, null, null);
    }
    
    public ExportXMLButton(String msg, ImageIcon icon, ActionListener listener, Mediator med){
        super(msg);
        setIcon(icon);
        addActionListener(listener);
        this.med = med;
    }
    
    @Override
    public void execute() {
        logger.info("execute");
        med.exportXML();
    }
}
