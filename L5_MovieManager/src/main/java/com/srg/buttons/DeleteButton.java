/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.buttons;

import com.srg.core.Command;
import com.srg.core.Mediator;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.apache.log4j.Logger;

/**
 *
 * @author administrator
 */
public class DeleteButton extends JButton implements Command{
    
    private Mediator med;
    final static Logger logger = Logger.getLogger(DeleteButton.class);
    private JFrame frame;
    
    public DeleteButton(String msg, ImageIcon icon, JFrame frame){
        this(msg, icon, null, null, frame);
    }
    
    public DeleteButton(String msg, ImageIcon icon, ActionListener listener, Mediator med, JFrame frame){
        super(msg);
        setIcon(icon);
        addActionListener(listener);
        this.med = med;
        this.frame = frame;
        setEnabled(false);
    }
    
    @Override
    public void execute() {
        logger.info("execte");
        if (!med.deleteMovie()){
            JOptionPane.showMessageDialog(frame, "Movie not has deleted !","Confirmation", JOptionPane.WARNING_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(frame, "Movie has deleted !","Confirmation", JOptionPane.OK_CANCEL_OPTION);
        }
    }
}