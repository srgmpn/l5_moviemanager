/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.pojo;

import java.util.List;

/**
 *
 * @author administrator
 */
public class Director {
    private int id;
    private String name;
    private List<Movie> movies;
    
    public Director(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
    
    @Override
    public boolean equals(Object obj){
        
        if (!(obj instanceof Director) || (obj == null))
            return false;
        
        if (obj == this)
            return true;
        
        Director d = (Director)obj;
        
        if ((d.id != id) || !(d.name.equals(name)))
            return false;
        
        if ((d.getMovies() == null) && (movies == null))
            return true;
        
        if (d.getMovies().size() != movies.size())
            return false;
        
        for (int i = 0; i < movies.size(); ++i){
            if (!d.getMovies().get(i).equals(movies.get(i)))
                return false;
        }
        return true;
    }  
    
    @Override
    public String toString(){
        String s;
        s = "["+id+","+name+":";
        if (movies != null){
            for (Movie m : movies){
                s += m.toString() + " ";
            }
        }
        s += "]";
        return s;
    }
}
