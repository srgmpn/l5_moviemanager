/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.pojo;

import java.util.List;

/**
 *
 * @author administrator
 */
public class Actor {
    private int id;
    private String name;
    private List<Movie> movies;
    
    public Actor(){
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
    
    @Override
    public boolean equals(Object obj){
        
        if (!(obj instanceof Actor) || (obj == null))
            return false;
        
        if (obj == this)
            return true;
        
        Actor a = (Actor)obj;
        
        if ((a.id != id) || !(a.name.equals(name)))
            return false;
        
        if ((a.getMovies() == null) && (movies == null))
            return true;
        
        if (a.getMovies().size() != movies.size())
            return false;
        
        for (int i = 0; i < movies.size(); ++i){
            if (!a.getMovies().get(i).equals(movies.get(i)))
                return false;
        }
        return true;
    }  
    
    @Override
    public String toString(){
        String s;
        s = "["+id+","+name+":";
        if (movies != null){
            for (Movie m : movies){
                s += m.toString() + " ";
            }
        }
        s += "]";
        return s;
    }
}
