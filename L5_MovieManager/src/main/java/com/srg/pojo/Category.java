/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.pojo;

import java.util.List;

/**
 *
 * @author administrator
 */
public class Category {
    private int id;
    private String category;
    private List<Movie> movies;
    
    public Category(){}
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    } 
    
    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
    
    @Override
    public boolean equals(Object obj){
        
        if (!(obj instanceof Category) || (obj == null))
            return false;
        
        if (obj == this)
            return true;
        
        Category c = (Category)obj;
        
        if ((c.id != id) || !(c.category.equals(category)))
            return false;
        
        if ((c.getMovies() == null) && (movies == null))
            return true;
        
        if (c.getMovies().size() != movies.size())
            return false;
        
        for (int i = 0; i < movies.size(); ++i){
            if (!c.getMovies().get(i).equals(movies.get(i)))
                return false;
        }
        return true;
    }  
    
    @Override
    public String toString(){
        String s;
        s = "["+id+","+category+":";
        if (movies != null){
            for (Movie m : movies){
                s += m.toString() + " ";
            }
        }
        s += "]";
        return s;
    }
}
