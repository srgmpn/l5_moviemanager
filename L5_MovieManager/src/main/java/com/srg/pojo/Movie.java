/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.pojo;

/**
 *
 * @author administrator
 */
public class Movie {
    
    private int id;
    private String title;
    private String imbd;
    private String year;
    private String runtime;
    private String lang;
    private int idDirector;
    private String jsonUrl;
    
    public Movie(){
        
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImbd() {
        return imbd;
    }

    public void setImbd(String imbd) {
        this.imbd = imbd;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }
    
    public void setIdDirector(int idDirector){
        this.idDirector = idDirector;
    }
    
    public int getIdDirector(){
        return idDirector;
    }
    
    public String getJsonUrl(){
        return jsonUrl;
    }
    
    public void setJsonUrl(String jsonUrl){
        this.jsonUrl = jsonUrl;
    }
    
    @Override
    public boolean equals(Object obj){
        
        if (!(obj instanceof Movie) || (obj == null))
            return false;
        
        if (this == obj)
            return true;
        
        Movie m = (Movie)obj;
        
        if (m.id != id || (!m.imbd.equals(imbd)) ||  
            (!m.lang.equals(lang)) || (!m.year.equals(year)) ||
            (!m.runtime.equals(runtime)) || (!m.title.equals(title)))
            return false;

        return true;
    }
    
    @Override
    public String toString(){
        return "["+id +","+title+","+imbd+","+year+","+runtime+","+lang+","+idDirector+"]";        
    }
}
