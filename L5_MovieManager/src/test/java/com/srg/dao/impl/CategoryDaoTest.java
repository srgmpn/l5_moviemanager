/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.dao.impl;

import com.srg.pojo.Category;
import com.srg.pojo.Movie;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author administrator
 */
public class CategoryDaoTest {

    /**
     * Test of insert method, of class CategoryDao.
     */
    @Test
    public void testInsert() throws Exception {
      /*  System.out.println("insert");
        Category c = new Category();
        c.setCategory("Sci-Fi");
        
        CategoryDao instance = new CategoryDao();
        instance.insert(c);*/

    }

    /**
     * Test of get method, of class CategoryDao.
     */
    @Test
    public void testGet() throws Exception {
        System.out.println("get");
        CategoryDao instance = new CategoryDao();
        
        Category expResult = new Category();
        Category result = instance.get("Action");
        
        expResult.setId(1);
        expResult.setCategory("Action");
        
        assertEquals(expResult, result);

    }

    /**
     * Test of getAll method, of class CategoryDao.
     */
    @Test
    public void testGetAll() throws Exception {
        System.out.println("getAll");
      /*  CategoryDao instance = new CategoryDao();
        List<Category> expResult = new ArrayList<>();
        List<Category> result = instance.getAll();
        
        String[] categories = new String[]{"Action","Thriller","Crime","Drama",
                                    "Adventure","Fantasy","Horror"};
        
        int i = 0;
        for (String cat : categories) {
            Category c = new Category();
            c.setId(++i);
            c.setCategory(cat);
            c.setMovies(null);
            expResult.add(c);
        }        
        
        assertEquals(expResult, result);*/

    }

    /**
     * Test of getAllMovie method, of class CategoryDao.
     */
    @Test
    public void testGetAllMovie() throws Exception {
        System.out.println("getAllMovie");
/*
        CategoryDao instance = new CategoryDao();
        Category expResult = new Category();
        Category result = instance.getAllMovie("Action");
        
        System.out.println(result.toString());
        
        expResult.setId(1);
        expResult.setCategory("Action");
        
        List<Movie> list = new ArrayList<>();
        Movie m = new Movie();
        m.setId(1);
        m.setTitle("Furious Seven");
        m.setImbd("tt2820852");
        m.setLang("English");
        m.setRuntime("137 min");
        m.setYear("2015");
        m.setIdDirector(1);

        list.add(m);        
        expResult.setMovies(list);
        
        assertEquals(expResult, result);*/

    }

    /**
     * Test of delete method, of class CategoryDao.
     */
    @Test
    public void testDelete() throws Exception {/*
        System.out.println("delete");
        Category t = new Category();
        t.setId(1);
        t.setCategory("Drama");
        CategoryDao instance = new CategoryDao();
        instance.delete(t);
*/
    }
    
}
