/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.dao.impl;

import com.srg.pojo.Director;
import com.srg.pojo.Movie;
import java.util.ArrayList;
import java.util.List;
import org.junit.*;
import static org.junit.Assert.assertEquals;



/**
 *
 * @author administrator
 */
public class DirectorDaoTest {
    
    public DirectorDaoTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of insert method, of class DirectorDao.
     */
    @Test
    public void testInsert() throws Exception {/*
        System.out.println("insert");
        DirectorDao instance = new DirectorDao();
        Director d = new Director();
        d.setName("Alex Garland");
        instance.insert(d);*/
    }

    /**
     * Test of get method, of class DirectorDao.
     */
    @Test
    public void testGet() throws Exception {
       /* System.out.println("get");
        DirectorDao dao = new DirectorDao();
        Director result = dao.get("Claudia Llosa");
        Director expResult = new Director();
        expResult.setId(2);
        expResult.setName("Claudia Llosa");
        
        assertEquals(expResult, result);*/
    }

    /**
     * Test of getAll method, of class DirectorDao.
     */
    @Test
    public void testGetAll() throws Exception {
       /* System.out.println("getAll");
        
        DirectorDao dao = new DirectorDao();
        
        List<Director> result = dao.getAll();     
        List<Director> expResult = new ArrayList<>();
        
        Director d = new Director();
        d.setId(1);
        d.setName("James Wan");
        d.setMovies(null);
        expResult.add(d);
      
        d = new Director();
        d.setId(2);
        d.setName("Claudia Llosa");
        expResult.add(d);
        
        d = new Director();
        d.setId(3);
        d.setName("Peter Jackson");
        expResult.add(d);
        
        assertEquals(expResult.size(), result.size());
        
        for (int i = 0; i < expResult.size(); ++i){
            assertEquals(expResult.get(i), result.get(i));
        }
*/
    }

    /**
     * Test of delete method, of class DirectorDao.
     */
    @Test
    public void testDelete() throws Exception {
     //   System.out.println("delete");
    }

    /**
     * Test of getAllMovie method, of class DirectorDao.
     */
    @Test
    public void testGetAllMovie() throws Exception {
     /*   System.out.println("getAllMovie");
        DirectorDao dao = new DirectorDao();
        Director result = dao.getAllMovie("James Wan");
        
        Director expResult = new Director();
        expResult.setId(1);
        expResult.setName("James Wan");
        
        List <Movie> list  = new ArrayList<>();
        Movie m = new Movie();
        m.setId(1);
        m.setTitle("Furious Seven");
        m.setImbd("tt2820852");
        m.setLang("English");
        m.setRuntime("137 min");
        m.setYear("2015");
        
        list.add(m);
         
        m = new Movie();
        m.setId(4);
        m.setTitle("The Conjuring");
        m.setImbd("tt1457767");
        m.setLang("English");
        m.setRuntime("112 min");
        m.setYear("2013");
        list.add(m);
        expResult.setMovies(list);
        
        assertEquals(expResult, result);*/
    }
    
}
