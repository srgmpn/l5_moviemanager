/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.httpclient;

import com.srg.restclient.IMDBHttpClient;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author administrator
 */
public class IMDBHttpClientTest {
    

    /**
     * Test of sendRequest method, of class IMDBHttpClient.
     */
    @Test
    public void testSendRequest() throws Exception {
        System.out.println("sendRequest");
        String url = "http://www.omdbapi.com/?i=tt3398260&plot=short&r=json";
        IMDBHttpClient instance = new IMDBHttpClient();
        String expResult = "{\"Title\":\"When Marnie Was There\","
                + "\"Year\":\"2014\",\"Rated\":\"PG\",\"Released\""
                + ":\"19 Jul 2014\",\"Runtime\":\"103 min\",\"Genre\""
                + ":\"Animation, Drama\",\"Director\":\"Hiromasa Yonebayashi\","
                + "\"Writer\":\"Joan G. Robinson (novel), Keiko Niwa (screenplay),"
                + " Masashi Ando (screenplay), Hiromasa Yonebayashi (screenplay), "
                + "David Freedman (screenplay)\",\"Actors\":\"Sara Takatsuki, Kasumi"
                + " Arimura, Nanako Matsushima, Susumu Terajima\",\"Plot\":\"A young "
                + "girl is sent to the country for health reasons, where she meets an "
                + "unlikely friend in the form of Marnie, a young girl with flowing blonde "
                + "hair. As the friendship unravels it is ...\",\"Language\":\"Japanese\",\"Country\":\"Japan\","
                + "\"Awards\":\"1 nomination.\",\"Poster\":"
                + "\"http://ia.media-imdb.com/images/M/MV5BMTQ1ODY3MTc5MF5BMl5BanBnXkFtZTgwOTUyNDI2NTE@._V1_SX300.jpg\","
                + "\"Metascore\":\"N/A\",\"imdbRating\":\"7.8\",\"imdbVotes\":\"1490\",\"imdbID\":\"tt3398268\",\"Type\":\"movie\","
                + "\"Response\":\"True\"}";
        String resultImdb = instance.sendRequest(url);
        
        System.out.println(resultImdb + "  <--->  " + resultImdb.length());
        
        //assertEquals(expResult, resultImdb);
        
        url = "http://www.omdbapi.com/?t=When+Marnie+Was+There&y=2014&plot=short&r=json";
        
        String resultNameYear = instance.sendRequest(url);
        
        System.out.println(resultNameYear.length());

        System.out.println(expResult.length());

        //assertEquals(expResult, resultNameYear);
    }
    
}
