/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.srg.dates.json;

import com.srg.pojo.Actor;
import com.srg.pojo.Category;
import com.srg.pojo.Director;
import com.srg.pojo.Movie;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author administrator
 */
public class UrlParserTest {
     String urlJsonFormat = "{\"Title\":\"Ex Machina\",\"Year\":\"2015\",\"Rated\":\"R\",\"Released\":\"10 Apr 2015\",\"Runtime\":\"108 min\",\"Genre\":\"Drama, Sci-Fi\",\"Director\":\"Alex Garland\",\"Writer\":\"Alex Garland\",\"Actors\":\"Domhnall Gleeson, Corey Johnson, Oscar Isaac, Alicia Vikander\",\"Plot\":\"A young programmer is selected to participate in a breakthrough experiment in artificial intelligence by evaluating the human qualities of a breathtaking female A.I.\",\"Language\":\"English\",\"Country\":\"UK\",\"Awards\":\"1 win & 1 nomination.\",\"Poster\":\"http://ia.media-imdb.com/images/M/MV5BMTUxNzc0OTIxMV5BMl5BanBnXkFtZTgwNDI3NzU2NDE@._V1_SX300.jpg\",\"Metascore\":\"78\",\"imdbRating\":\"8.1\",\"imdbVotes\":\"15625\",\"imdbID\":\"tt0470752\",\"Type\":\"movie\",\"Response\":\"True\"}";
    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of parseUrlJsonFormat method, of class UrlParser.
     */
    @Test
    public void testParseUrlJsonFormat() {

       
        UrlParser instance = new UrlParser(null);
        instance.parseUrlJsonFormat(urlJsonFormat);

        String[] l = "Alexxandr Mkhai".split(",");
        for (String s : l) {
            System.out.println(s.trim());
        }

    }

}
